﻿using System.Text.RegularExpressions;

namespace System {

    ///<summary>
    ///Defines a textual boolean value
    ///</summary>
    ///<remarks></remarks>
    public enum BooleanText {
        AcceptedDeclined,
        ActiveInactive,
        CheckedUnchecked,
        CorrectIncorrent,
        EnabledDisabled,
        OnOff,
        YesNo
    }

    public static class BooleanExtension {
        /// <summary>
        /// Assesses a boolean value and returns a textual value
        /// </summary>
        /// <param name="value"></param>
        /// <param name="text">The textual value to be returned</param>
        /// <returns>A textual representation of the boolean value</returns>
        /// <remarks></remarks>
        public static string ToString(this bool value, BooleanText text) {
            MatchCollection matches = Regex.Matches(text.ToString(), "[A-Z][a-z]+");
            return value.ToString(matches[0].Value, matches[1].Value);
        }


        /// <summary>
        /// Assesses a boolean value and returns a specified word in respect to true or false
        /// </summary>
        /// <param name="value"></param>
        /// <param name="trueValue">The textual value to be returned if the current boolean is true</param>
        /// <param name="falseValue">The textual value to be returned if the current boolean is false</param>
        /// <returns>A string representation of the current boolean value</returns>
        /// <remarks></remarks>
        public static string ToString(this bool value, string trueValue, string falseValue) {
            return (value) ? trueValue : falseValue;
        }

        public static string ToString(this bool value) {
            return (value) ? "1" : "0";
        }

    }
}
