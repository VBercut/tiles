﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.EtD.Math;

namespace Assets.EtD.Serializable {

    [Serializable]
    public struct AutoTileTemplateSerializable {

        public int Id;
        public IntVector2 Position;
        public string Name;
        public List<int> AutoTilePairs;

    }
}
