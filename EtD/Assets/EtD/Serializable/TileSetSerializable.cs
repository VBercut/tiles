﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.EtD.Serializable {

    [System.Serializable]
    public struct TileSetSerializable {

        public Material Material;

        public string Name;

        public List<AutoTileTemplateSerializable> AutoTileTemplates;

        public EtDTileSet Deserializable(EtDTileMap tileMap) {
            var result = new EtDTileSet(Material, tileMap, Name);

            foreach (var template in AutoTileTemplates) {
                result.AddAutoTileTemplate(template); 
            }     

            return result;
        }
    }
}