﻿using System.Collections.Generic;

namespace Assets.EtD.Serializable {

    [System.Serializable]
    public struct AutoTilePairSerializable {
        public int Id;
        public int First;
        public int Second;
        public string Name;
        public List<EtDTileContainer> Tiles;

        public EtDAutoTilePair Deserializable(EtDTileMap tileMap) {
            return new EtDAutoTilePair(tileMap, Id, First, Second, Name, Tiles);
        }
    }
}