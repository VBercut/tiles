﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.EtD.Math;
using Assets.EtD.Serializable;
using Assets.EtD.Utils;
using UnityEngine;

namespace Assets.EtD {

    public class EtDAutoTilePair : ISource {

        private const int TilesCount = 16;

        private EtDAutoTileTemplate _first;

        private EtDAutoTileTemplate _second;

        private List<EtDTileContainer> _tiles;

        public List<EtDTileContainer> Tiles
        {
            get { return _tiles ?? (_tiles = new List<EtDTileContainer>()); }
        }


        public EtDAutoTileTemplate First
        {
            get { return _first ?? (_first = _tileMap.FindTemplate(_firstId)); }
        }

        public int FirstId {
            get { return _firstId; }
        }

        public int SecondId {
            get { return _secondId; }
        }

        public EtDAutoTileTemplate Second {
            get { return _second ?? (_second = _tileMap.FindTemplate(_secondId)); }
            set {
                if (value != null) {
                    _second = value;
                }
            }
        }

        public string Name;

        private readonly EtDTileMap _tileMap;

        private readonly int _id;
        private readonly int _firstId;
        private readonly int _secondId;

        public int ID
        {
            get { return _id; }
        }

        public EtDAutoTilePair(EtDTileMap tileMap, EtDAutoTileTemplate template, IntVector2 tileSize, float pixelToUnit) : this(tileMap, template, template, tileSize, pixelToUnit) { }

        public EtDAutoTilePair(EtDTileMap tileMap, EtDAutoTileTemplate first, EtDAutoTileTemplate second, IntVector2 tileSize, float pixelToUnit) {
            _first = first;
            _second = second;

            for (int i = 0; i < TilesCount; i++) {
                Tiles.Add(new EtDTileContainer(tileSize, pixelToUnit));
            }

            Name = EtDEditorDictionary.GetWord(EtDEditorDictionary.NewTemplate);

            _tileMap = tileMap;
            _id = _tileMap.Intervals.UniqueNumber(EtDIntervals.AutoTilePair);
        }

        public EtDAutoTilePair(EtDTileMap tileMap, int id, int first, int second, string name, List<EtDTileContainer> tiles) {
            _tileMap = tileMap;
            _firstId = first;
            _secondId = second;
            _tiles = tiles;
            _id = id;

            Name = name;
        }

        public void ChangeTileMaterial(int tileNumber, Material material, IntVector2 position) {
            EtDTileContainer tileContainer = Tiles[tileNumber];
            if (tileContainer == null) return;

            for (int i = 0; i < 4; i++) {
               tileContainer.ChangeCornerMaterial(i, material, tileContainer.CalculateCornerTexturePosition(i, new IntVector2(position.x * 2, position.y * 2)));
            }
        }

        public void ChangeTileMaterial(int tileNumber, int cornerNumber, Material material, IntVector2 position) {
            EtDTileContainer tileContainer = Tiles[tileNumber];
            if(tileContainer == null) return;

            tileContainer.ChangeCornerMaterial(cornerNumber, material, position);
        }


        public override bool Equals(object obj) {
            if (!(obj is EtDAutoTilePair)) return false;

            var other = (EtDAutoTilePair) obj;

            return (other._first == _first && other._second == _second) ||
                   (other._second == _first && other._first == _second);
        }

        public override int GetHashCode() {
            return ID;
        }

        public EtDAutoTileTemplate GetAnotherTemplate(EtDAutoTileTemplate template) {
            return template.Equals(_first) ? _second : _first;
        }

        public AutoTilePairSerializable Serializable()
        {
            var result = new AutoTilePairSerializable()
            {
                Id = ID,
                First = First.ID,
                Second = Second.ID,
                Name = Name,
                Tiles = Tiles
            };

            return result;
        }

        public EtDTileContainer FindTile(IEnumerable<int> result, int id)
        {
            var temp = new bool[4];
            var enumerable = result as int[] ?? result.ToArray();
            for (int i = 0; i < enumerable.Count(); i++) {
                if (_firstId == _secondId) {
                    temp[i] = enumerable[i] != id;
                } else {
                    if (enumerable[i] == id && id == FirstId)
                        temp[i] = false;

                    if (enumerable[i] == id && id == SecondId)
                        temp[i] = true;

                    if (enumerable[i] != id && id == FirstId)
                        temp[i] = true;

                    if (enumerable[i] != id && id == SecondId)
                        temp[i] = false;
                }   
            }

            // 0000
            if (!temp[0] && !temp[1] && !temp[2] && !temp[3]) {
                return Tiles[0];
            }
            // 0011
            if (!temp[0] && !temp[1] && temp[2] && temp[3]) {
                return Tiles[1];
            }

            // 0101
            if (!temp[0] && temp[1] && !temp[2] && temp[3]) {
                return Tiles[2];
            }

            // 1010
            if (temp[0] && !temp[1] && temp[2] && !temp[3])
            {
                return Tiles[3];
            }

            // 1111
            if (temp[0] && temp[1] && temp[2] && temp[3])
            {
                return Tiles[4];
            }

            // 1100
            if (temp[0] && temp[1] && !temp[2] && !temp[3]) {
                return Tiles[5];
            }

            // 0110
            if (!temp[0] && temp[1] && temp[2] && !temp[3])
            {
                return Tiles[6];
            }

            // 1001
            if (temp[0] && !temp[1] && !temp[2] && temp[3])
            {
                return Tiles[7];
            }

            // 0001
            if (!temp[0] && !temp[1] && !temp[2] && temp[3]) {
                return Tiles[8];
            }

            // 0010
            if (!temp[0] && !temp[1] && temp[2] && !temp[3])
            {
                return Tiles[9];
            }

            // 1110
            if (temp[0] && temp[1] && temp[2] && !temp[3])
            {
                return Tiles[10];
            }

            // 1101
            if (temp[0] && temp[1] && !temp[2] && temp[3])
            {
                return Tiles[11];
            }

            // 0100
            if (!temp[0] && temp[1] && !temp[2] && !temp[3])
            {
                return Tiles[12];
            }

            // 1000
            if (temp[0] && !temp[1] && !temp[2] && !temp[3]) {
                return Tiles[13];
            }

            // 1011
            if (temp[0] && !temp[1] && temp[2] && temp[3]) {
                return Tiles[14];
            }

            // 0111
            if (!temp[0] && temp[1] && temp[2] && temp[3]) {
                return Tiles[15];
            }


            return Tiles[0];
        }
    }
}
