﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.EtD.Math;
using Assets.EtD.Serializable;
using Assets.EtD.Utils;
using UnityEngine;

namespace Assets.EtD {

    public class EtDAutoTileTemplate : ISource {

        private readonly int _x;
        private readonly int _y;

        private readonly EtDTileSet _tileSet;
        private List<EtDAutoTilePair> _autoTilePairs;

        private readonly int _id;
        private readonly List<int> _autoTilePairIds;

        public int ID {
            get { return _id; }
        }

        public List<EtDAutoTilePair> AutoTilePairs {
            get
            {
                if (_autoTilePairs == null) {
                    _autoTilePairs = new List<EtDAutoTilePair>();

                    foreach (var pairId in _autoTilePairIds) {
                        EtDAutoTilePair currentPair = _tileSet.TileMap.FindPair(pairId);
                        _autoTilePairs.Add(currentPair);
                    }
                }
                
                return _autoTilePairs;
            }
        }

        public string Name;

        public Texture Texture {
            get { return _tileSet.MainTexture; }
        }

        private Rect _textureCoords;

        public Rect TextureCoords {
            get {
                if (_tileSet.MainTexture != null) {
                   _textureCoords = EtDMeshUtils.TextureCoords(_tileSet.MainTexture, new IntVector2(_x, _y), _tileSet.TileMap.CellSize);

                }
                return _textureCoords;
            }
        }

        private int GetNewId
        {
            get
            {
                return _tileSet.TileMap.Intervals.UniqueNumber(EtDIntervals.AutoTileTemplate);
            }
        }

        public EtDAutoTileTemplate(int x, int y, EtDTileSet tileSet) : this(x, y, tileSet, "") {
        }

        public EtDAutoTileTemplate(int x, int y, EtDTileSet tileSet, string name) {
            _x = x;
            _y = y;
            _tileSet = tileSet;

            _autoTilePairs = new List<EtDAutoTilePair>();

            Name = name;
            _id = GetNewId;
        }

        public EtDAutoTileTemplate(AutoTileTemplateSerializable serializable, EtDTileSet tileSet) {
            _id = serializable.Id;
            _x = serializable.Position.x;
            _y = serializable.Position.y;
            _tileSet = tileSet;

            _autoTilePairIds = serializable.AutoTilePairs;

            Name = serializable.Name;
        }

        private IntVector2 _position;

        public IntVector2 Position {
            get { return _position ?? (_position = new IntVector2(_x, _y)); }
        }

        public List<int> AutoTilePairsSerialization {
            get {
                if(AutoTilePairs.Count == 0) return new List<int>();

                // return AutoTilePairs.Select(pair => pair.ID).ToList();

                return AutoTilePairs.Select(pair => ID).ToList();
            }
        }

        public override bool Equals(object obj) {
            if (!(obj is EtDAutoTileTemplate)) return false;
            var other = (EtDAutoTileTemplate) obj;

            return other._tileSet.Equals(_tileSet) && other._x == _x && other._y == _y;
        }

        public override int GetHashCode() {
            return (new StringBuilder()).Append(_x).Append(_y).ToString().GetHashCode();
        }

        public override string ToString() {
            return (new StringBuilder()).Append(_tileSet).Append(" position: x = ").Append(_x).Append(" y = ").Append(_y).ToString();
        }

        public AutoTileTemplateSerializable Serializable() {
            var result = new AutoTileTemplateSerializable {
                Id = ID,
                Position = new IntVector2(Position),
                Name = Name,
                AutoTilePairs = AutoTilePairsSerialization
            };

            return result;
        }
    }
}
