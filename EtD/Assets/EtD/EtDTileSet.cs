﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Assets.EtD.Math;
using Assets.EtD.Serializable;
using Assets.EtD.Utils;
using UnityEngine;

namespace Assets.EtD {
    public class EtDTileSet {

        private Material _material;

        public Material Material {
            get { return _material; }
        }

        private List<EtDAutoTileTemplate> _templates;

        public List<EtDAutoTileTemplate> Templates
        {
            get { return _templates ?? (_templates = new List<EtDAutoTileTemplate>()); }
        }


        private Dictionary<IntVector2, EtDAutoTileTemplate> _templatesDictionary;

        private Dictionary<IntVector2, EtDAutoTileTemplate> TemplatesDictionary
        {
            get
            {
                if (_templatesDictionary == null) {
                    _templatesDictionary = new Dictionary<IntVector2, EtDAutoTileTemplate>();

                    foreach (var template in Templates) {
                        _templatesDictionary[template.Position] = template;
                    }
                }
                return _templatesDictionary;
            }
        }


        public string ShaderName {
            get { return _material.shader.name; }
        }

        public string TextureName {
            get { return MainTexture ? MainTexture.name : "null"; }
        }

        public Texture MainTexture
        {
            get { return _material.mainTexture; }
            set { _material.mainTexture = value; }
        }

        public string Name;

        private EtDTileMap _parent;

        public EtDTileMap TileMap
        {
            get { return _parent; }
        }

        public EtDTileSet(Material material, EtDTileMap parent, string name = "") {
            _material = material;
            Name = name ?? TextureName;
            _parent = parent;
        }

        public override bool Equals(object obj) {
            if (!(obj is EtDTileSet)) return false;

            var other = (EtDTileSet) obj;

            return ShaderName == other.ShaderName && TextureName == other.TextureName;
        }

        public override int GetHashCode() {
            return ((new StringBuilder()).Append(ShaderName).Append(TextureName)).GetHashCode();
        }

        public override string ToString() {
            return (new StringBuilder()).Append(Name).Append(": ").Append("Texture = ").Append(TextureName).Append(" Shader = ").Append(ShaderName).ToString();
        }

        public EtDAutoTileTemplate FindAutoTileTemplate(IntVector2 currentTilePosition){
            if (!TemplatesDictionary.ContainsKey(currentTilePosition)) return null;

            return TemplatesDictionary[currentTilePosition];
        }

        public void RemoveAutoTileTemplate(IntVector2 currentTilePosition) {
            if (!TemplatesDictionary.ContainsKey(currentTilePosition)) return;

            EtDAutoTileTemplate template = TemplatesDictionary[currentTilePosition];
            if(template == null) return;

            TemplatesDictionary.Remove(currentTilePosition);
            Templates.Remove(template);
        }

        public void AddAutoTileTemplate(IntVector2 currentTilePosition) {
            var template = new EtDAutoTileTemplate(currentTilePosition.x, currentTilePosition.y, this, EtDEditorDictionary.GetWord(EtDEditorDictionary.NewTemplate));
            AddAutoTileTemplate(template);
        }

        public void AddAutoTileTemplate(AutoTileTemplateSerializable templateSerializable) {
            AddAutoTileTemplate(new EtDAutoTileTemplate(templateSerializable, this));
        }

        public void AddAutoTileTemplate(EtDAutoTileTemplate template) {
            TemplatesDictionary[new IntVector2(template.Position)] = template;
            Templates.Add(template);

            _parent.AddAutoTileTemplate(template);
        }

        public TileSetSerializable Serializable() {
            var result = new TileSetSerializable
            {
                Name = Name,
                Material = Material,
                AutoTileTemplates = new List<AutoTileTemplateSerializable>()
            };

            foreach (var template in Templates) {
               result.AutoTileTemplates.Add(template.Serializable()); 
            }

            return result;
        }

    } 
}
