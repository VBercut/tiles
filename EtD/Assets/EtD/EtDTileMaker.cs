﻿using System.Security.Cryptography.X509Certificates;
using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEngine;

namespace Assets.EtD {
    [ExecuteInEditMode]
    public class EtDTileMaker : MonoBehaviour {

        private IntVector2 _selectStart;

        public IntVector2 SelectStart {
            get { return _selectStart ?? (_selectStart = new IntVector2((int)Position.x, (int)Position.y)); }

            set { SelectStart.Set(value); }
        }

        private IntVector2 _selectEnd;

        public IntVector2 SelectEnd
        {
            get { return _selectEnd ?? (_selectEnd = new IntVector2((int)Position.x, (int)Position.y)); }

            set {
                SelectEnd.Set(value);
            }
        }

        private IntVector2 _selectEndOld;

        public IntVector2 SelectEndOld {

            get { return _selectEndOld ?? (_selectEndOld = new IntVector2((int)Position.x, (int)Position.y)); }

            set { SelectEndOld.Set(value); }
        }



        private IntVector2 _selectStartOnPallete;

        public IntVector2 SelectStartOnPallete
        {
            get { return _selectStartOnPallete ?? (_selectStartOnPallete = new IntVector2()); }

            set {
                SelectStartOnPallete.Set(value);
            }
        }

        private IntVector2 _selectEndOnPallete;

        public IntVector2 SelectEndOnPallete
        {
            get { return _selectEndOnPallete ?? (_selectEndOnPallete = new IntVector2()); }

            set {
                SelectEndOnPallete.Set(value);
            }
        }


        public IntVector2 SelectedTilesOnPallete {
            get { return new IntVector2(System.Math.Abs(SelectStartOnPallete.x - SelectEndOnPallete.x) + 1, System.Math.Abs(SelectStartOnPallete.y - SelectEndOnPallete.y) + 1); }
        }

        public IntVector2 SelectedTiles {
            get { return new IntVector2(System.Math.Abs(SelectStart.x - SelectEnd.x) + 1, System.Math.Abs(SelectStart.y - SelectEnd.y) + 1); }
        }

        public IntVector2 StartPositionSelectOnPallete {
            get { return new IntVector2(System.Math.Min(SelectStartOnPallete.x, SelectEndOnPallete.x), System.Math.Min(SelectStartOnPallete.y, SelectEndOnPallete.y)); }
        }

        public IntVector2 StartPositionSelect {
            get { return new IntVector2(System.Math.Min(SelectStart.x, SelectEnd.x), System.Math.Min(SelectStart.y, SelectEnd.y)); }
        }

        public IntVector2 EndPositionSelect{
            get { return new IntVector2(System.Math.Max(SelectStart.x, SelectEnd.x), System.Math.Max(SelectStart.y, SelectEnd.y)); }
        }

        public Rect SelectedTilesBoundOnPallete {
            get {
                IntVector2 start = StartPositionSelectOnPallete;
                IntVector2 tiles = SelectedTilesOnPallete;

                return new Rect(start.x, start.y, tiles.x, tiles.y);
            }
        }

        public bool StartDrag;
        public bool StartDragDraw;

        public EtDTileMap TileMap;
        public Material Material;

        public Vector2 Position;

        private States _state;
        private States _previousState;

        private Transform _templateTilesContainer;
        private Transform _selectedTilesContainer;

        private const string TemplateTilesContainerName = "TemplateTiles";
        private const string SelectedTilesContainerName = "SelectedTiles";

        public enum States
        {
            Draw,
            MassDraw,
            Erase
        }

        public void Start() {
            _templateTilesContainer = EtDGameObjectUtils.AddChild(transform, TemplateTilesContainerName, true);
            _selectedTilesContainer = EtDGameObjectUtils.AddChild(transform, SelectedTilesContainerName, true); 
        }

        public void ToGrid(Vector3 coords, Vector2 tileSize) {
            Position = new Vector2(Mathf.Floor(coords.x / tileSize.x), Mathf.Floor(coords.y / tileSize.y));

            transform.localPosition = new Vector3(Position.x * tileSize.x, Position.y * tileSize.y, 0);

            Vector3 position = transform.position;
            position.z = -1;

            transform.position = position;

            Position.y *= -1;
        }

        public void BuildMesh() {

            switch (_state)
            {
                case States.Draw:
                    IntVector2 selectedTiles = SelectedTilesOnPallete;
                    IntVector2 startPosition = StartPositionSelectOnPallete;

                    _templateTilesContainer = EtDGameObjectUtils.AddChild(transform, TemplateTilesContainerName, true);
                    var tilesContainer = _templateTilesContainer.gameObject.AddComponent<EtDTilesContainer>();

                    tilesContainer.Sizes = selectedTiles;

                    for (int y = 0; y < selectedTiles.y; y++) {
                        for (int x = 0; x < selectedTiles.x; x++) {
                            tilesContainer.AddTile(Material, new IntVector2(startPosition.x + x, startPosition.y + y), TileMap.CellSize, TileMap.TileSize); 
                        }      
                    }
   
                    break;

                case States.MassDraw:

                    if (SelectEnd.Equals(_selectEndOld)) return;

                    var oldDirection = new IntVector2(System.Math.Sign(_selectEndOld.x - SelectStart.x),
                                                      System.Math.Sign(_selectEndOld.y - SelectStart.y));

                    var currentDirection = new IntVector2(System.Math.Sign(SelectEnd.x - SelectStart.x),
                                                          System.Math.Sign(SelectEnd.y - SelectStart.y));

                    var oldCentralCoords = new IntVector2(_selectEndOld.x - SelectStart.x, _selectEndOld.y - SelectStart.y);
                    var currentCentralCoords = new IntVector2(SelectEnd.x - SelectStart.x, SelectEnd.y - SelectStart.y);


                    var selectedTilesContainer = _selectedTilesContainer.gameObject.GetComponent<EtDTilesContainer>();

                    var addedTiles = new IntVector2();
                    var removedTiles = new IntVector2();

                    var delta = new IntVector2(System.Math.Abs(currentCentralCoords.x) - System.Math.Abs(oldCentralCoords.x),
                                               System.Math.Abs(currentCentralCoords.y) - System.Math.Abs(oldCentralCoords.y));

                    if (oldDirection.x == currentDirection.x || oldDirection.x == 0 || currentDirection.x == 0) {
                        addedTiles.x = (delta.x > 0) ? delta.x : 0;
                        removedTiles.x = (delta.x < 0) ? System.Math.Abs(delta.x) : 0;
                    } else {
                        addedTiles.x = System.Math.Abs(currentCentralCoords.x);
                        removedTiles.x = System.Math.Abs(oldCentralCoords.x);
                    }


                    if (oldDirection.y == currentDirection.y || oldDirection.y == 0 || currentDirection.y == 0) {
                        addedTiles.y = (delta.y > 0) ? delta.y : 0;
                        removedTiles.y = (delta.y < 0) ? System.Math.Abs(delta.y) : 0;
                    } else {
                        addedTiles.y = System.Math.Abs(currentCentralCoords.y);
                        removedTiles.y = System.Math.Abs(oldCentralCoords.y);
                    }

                    for (int i = 0; i < removedTiles.x; i++) {
                        selectedTilesContainer.RemoveColumn();
                    }

                    for (int i = 0; i < removedTiles.y; i++) {
                        selectedTilesContainer.RemoveRow();
                    }

                    for (int i = 0; i < addedTiles.x; i++) {
                        selectedTilesContainer.AddColumn(Material, SelectedTilesBoundOnPallete, TileMap.CellSize, TileMap.TileSize, currentDirection);
                    }

                    for (int i = 0; i < addedTiles.y; i++) {
                        selectedTilesContainer.AddRow(Material, SelectedTilesBoundOnPallete, TileMap.CellSize, TileMap.TileSize, currentDirection);
                    }

                    _selectEndOld.Set(SelectEnd); 

                    break;
            }
        }

        public bool CheckState(States state) {
            return _state == state;
        }


        public void ChangeState(States state) {
            if(state == _previousState) return;

            _state = state;

            SelectStart = new IntVector2((int) Position.x, (int) Position.y);
            _selectEndOld = new IntVector2(SelectStart);
            SelectEnd = SelectStart;

            // Do actions

            switch (state) {
                case States.Draw:
                    _templateTilesContainer.gameObject.SetActive(true);
                    // Clear selected tiles
                    _selectedTilesContainer = EtDGameObjectUtils.AddChild(transform, SelectedTilesContainerName, true);
                break;

                case States.MassDraw:
                    RebuildSelectedTiles();
                break;

                case States.Erase:
                    _selectedTilesContainer.gameObject.SetActive(false);
                    break;
            }

            if (state == States.MassDraw || state == States.Erase) {
                // Hide template container, don't destroy
                _templateTilesContainer.gameObject.SetActive(false);
            }

            _previousState = state;
        }

        public void RebuildSelectedTiles() {
            _selectedTilesContainer = EtDGameObjectUtils.AddChild(transform, SelectedTilesContainerName, true);

            // Build selected tiles
            IntVector2 startPosition = StartPositionSelectOnPallete;
            var tilesContainer = _selectedTilesContainer.gameObject.AddComponent<EtDTilesContainer>();

            tilesContainer.Sizes = new IntVector2(1, 1);
            tilesContainer.AddTile(Material, new IntVector2(startPosition.x, startPosition.y), TileMap.CellSize, TileMap.TileSize); 
        }


        private void OnDrawGizmos() {

            var newPosition = new Vector2(transform.position.x, transform.position.y);

            var selectedTiles = new IntVector2();

            if (CheckState(States.Draw)) {
                selectedTiles = SelectedTilesOnPallete;
            }

            if (CheckState(States.MassDraw) || CheckState(States.Erase)) {
                selectedTiles = SelectedTiles;

                if (SelectStart.x > SelectEnd.x) {
                    newPosition.x -= TileMap.TileSize.x * (selectedTiles.x - 1);
                }

                if (SelectStart.y > SelectEnd.y) {
                    newPosition.y += TileMap.TileSize.y * (selectedTiles.y - 1);
                }
            }

            var meshSize = new Vector2(TileMap.TileSize.x * selectedTiles.x, TileMap.TileSize.y * selectedTiles.y);

            var borderPosition = new Vector3(newPosition.x + meshSize.x / 2, newPosition.y - meshSize.y / 2,
                                             transform.position.z);

            var borderSize = new Vector3(meshSize.x, meshSize.y);

            Gizmos.color = _state == States.Erase
                ? new Color(0.89f, 0.43f, 0.33f, 0.66f)
                : new Color(0.33f, 0.89f, 0.33f, 0.11f);

            Gizmos.DrawCube(borderPosition, borderSize);

            Gizmos.color = EtDDrawUtils.WhiteBorderColor;

            Gizmos.DrawWireCube(borderPosition, borderSize);
        }


        public void Draw(Transform layer) {
            if(layer == null) return;

            Transform container = _state == States.Draw ? _templateTilesContainer : _selectedTilesContainer;

            foreach (Transform child in container.transform) {
                var tile = child.GetComponent<Tile>();
                if(tile == null) continue;
                TileMap.AddTile(layer, tile);
            }
        }

        public void EraseTiles(Transform layer) {
            if (layer == null) return;

            var start = StartPositionSelect;
            var end = EndPositionSelect;

            for (int y = start.y; y <= end.y; y++) {
                for (int x = start.x; x <= end.x; x++) {
                    TileMap.RemoveTile(x, y, layer);
                }           
            }

        }
    }
}
