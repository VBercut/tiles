﻿using System;
using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEngine;

namespace Assets.EtD {
    public class Tile : MonoBehaviour {
        private MeshRenderer _meshRenderer;
        private MeshFilter   _meshFilter;

        public MeshRenderer MeshRenderer {
            get {
                return _meshRenderer ?? (_meshRenderer = gameObject.AddComponent<MeshRenderer>());
            }
        }

        public MeshFilter MeshFilter {
            get {
                return _meshFilter ?? (_meshFilter = gameObject.AddComponent<MeshFilter>());
            }
        }

        public IntVector2 Position;
        public IntVector2 TexturePosition;

        public Vector2 MeshSize;

        public Material Material {
            get { return MeshRenderer.sharedMaterial; }
        }

        public Mesh Mesh {
            get { return MeshFilter.sharedMesh; }
        }

        public void CreateTile(Material material, IntVector2 startPosition, IntVector2 tileSize, Vector2 meshSize) {
            MeshRenderer.sharedMaterial = material;
            MeshFilter.sharedMesh = EtDMeshUtils.BuildRectMesh(meshSize, material, startPosition, tileSize);

            MeshSize = new Vector2(meshSize.x, meshSize.y);
        }

    }
}
