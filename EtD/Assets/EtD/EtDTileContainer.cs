﻿
using System;
using System.Collections.Generic;
using Assets.EtD.Math;
using UnityEngine;

namespace Assets.EtD
{
    [Serializable]
    public class EtDTileContainer {

        public class Corner {
            public const int LeftTop = 0;
            public const int RightTop = 1;
            public const int LeftBottom = 2;
            public const int RightBottom = 3; 
        }

        [SerializeField]
        private List<EtDTile> _corners;

        [SerializeField]
        private IntVector2 _tileSize;

        [SerializeField]
        private Vector2 _tileMeshSize;

        public List<EtDTile> Corners {
            get { return _corners ?? (_corners = new List<EtDTile>()); }   
        }

        public EtDTile LeftTopCorner  {
            get { return Corners[0]; }
        }

        public EtDTile RightTopCorner
        {
            get { return Corners[1]; }
        }

        public EtDTile LeftBottomCorner
        {
            get { return Corners[2]; }
        }

        public EtDTile RightBottomCorner
        {
            get { return Corners[3]; }
        }


        public EtDTileContainer(IntVector2 tileSize, float pixelToUnit) {
            _tileSize = new IntVector2(tileSize.x / 2, tileSize.y / 2);

            _tileMeshSize = _tileSize / pixelToUnit;

            for (int i = 0; i < 4; i++) {
                Corners.Add(new EtDTile());
            }
        }

        public void ChangeCornerMaterial(int cornerNumber, Material material, IntVector2 position) {
            if(cornerNumber < 0 || cornerNumber >= 4) return;

            Corners[cornerNumber].BuildTile(material, position, _tileSize, _tileMeshSize);          
        }


        public IntVector2 CalculateCornerTexturePosition(int cornerNumber, IntVector2 position) {
            IntVector2 texturePosition = null;

            switch (cornerNumber)
            {
                case Corner.LeftTop:
                    texturePosition = new IntVector2(position);
                    break;

                case Corner.RightTop:
                    texturePosition = new IntVector2(position.x + 1, position.y);
                    break;

                case Corner.LeftBottom:
                    texturePosition = new IntVector2(position.x, position.y + 1);
                    break;

                case Corner.RightBottom:
                    texturePosition = new IntVector2(position.x + 1, position.y + 1);
                    break;
            }

            return texturePosition;
        }
    }
}
