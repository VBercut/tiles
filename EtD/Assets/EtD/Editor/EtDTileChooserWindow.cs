﻿using System.Collections.Generic;
using System.Linq;
using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;

namespace Assets.EtD.Editor {
    class EtDTileChooserWindow : EditorWindow {

        public delegate void Callback(IntVector2 pos, EtDTileSet tileSet);

        private IntVector2 _tileSize;
        private EtDTileMap _tileMap;

        private Callback _callback;

        private string[] _tileSetNames;

        private Vector2 _palettePos;
        private IntVector2 _currentTilePosition;

        public void Init(IntVector2 tileSize, EtDTileMap tileMap, Callback callback = null) {
            _tileSize = tileSize;
            _tileMap = tileMap;
            _callback = callback;

            _tileSetNames = tileMap.TileSets.Select(tileSet => tileSet.Name).ToArray();

            _currentTilePosition = new IntVector2(-1, -1);
        }

        private void OnGUI() {
            GUILayout.BeginVertical();

            DrawPallete();

            GUILayout.EndVertical();
        }

        private void DrawPallete() {
            _tileMap.CurrentTileSetChooserIndex = EditorGUILayout.Popup(EtDEditorDictionary.GetWord(EtDEditorDictionary.Tileset),
                                                                        _tileMap.CurrentTileSetChooserIndex, _tileSetNames);

            _tileMap.TileChooserScale = EditorGUILayout.Slider(EtDEditorDictionary.GetWord(EtDEditorDictionary.PalleteScale), _tileMap.TileChooserScale,
                                                               EtDTileMap.MinScale, EtDTileMap.MaxScale);

            EtDTileSet tileSet = _tileMap.TileSets[_tileMap.CurrentTileSetChooserIndex];
            if (tileSet == null) return;

            Texture tileSetTexture = tileSet.MainTexture;
            float palleteScale = _tileMap.TileChooserScale;

            var viewRect = new Rect(0, 0, tileSetTexture.width, tileSetTexture.height);
            viewRect.width *= palleteScale;
            viewRect.height *= palleteScale;

            _palettePos = EditorGUILayout.BeginScrollView(_palettePos);

            UnityEngine.GUI.DrawTexture(viewRect, tileSetTexture, ScaleMode.StretchToFill, true, palleteScale - 1);
            GUILayoutUtility.GetRect(viewRect.width, viewRect.height);
            DrawSelectedTiles(GUILayoutUtility.GetLastRect());

            EditorGUILayout.EndScrollView();

            HandleInput(GUILayoutUtility.GetLastRect());
        }


        private void DrawSelectedTiles(Rect area) {

            if (_currentTilePosition.x < 0 || _currentTilePosition.y < 0) return;

            Vector2 cellSize = _tileMap.TileChooserCellSize;

            cellSize.x = _tileSize.x * _tileMap.TileChooserScale;
            cellSize.y = _tileSize.y * _tileMap.TileChooserScale;


            var highlightRect = new Rect(area.x + _currentTilePosition.x * cellSize.x,
                                         area.y + _currentTilePosition.y * cellSize.y,
                                         cellSize.x,
                                         cellSize.y);

            EtDDrawUtils.DrawSolidRectangleWithOutline(highlightRect);
        }

        private void HandleInput(Rect area)
        {
            //int controlID = GUIUtility.GetControlID(FocusType.Passive, area);

            if (!area.Contains(Event.current.mousePosition)) return;

            switch (Event.current.type)
            {
                case EventType.MouseDown:
                    Vector2 localClickPosition = Event.current.mousePosition - new Vector2(area.x, area.y)
                        + new Vector2(_palettePos.x, _palettePos.y);

                    Vector2 cellSize = _tileMap.TileChooserCellSize;

                    cellSize.x = _tileSize.x * _tileMap.TileChooserScale;
                    cellSize.y = _tileSize.y * _tileMap.TileChooserScale;

                    _currentTilePosition.x = (int)(localClickPosition.x / cellSize.x);
                    _currentTilePosition.y = (int)(localClickPosition.y / cellSize.y);

                    if (Event.current.clickCount == 2) {
                        _callback(new IntVector2(_currentTilePosition),
                            _tileMap.TileSets[_tileMap.CurrentTileSetChooserIndex]);
                        Close();
                    }

                 break;
            }

        }


        private void Update() {
            Repaint();
        }

    }
}
