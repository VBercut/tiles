﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;

namespace Assets.EtD.Editor
{
    public class EtDAutoTileEditorWindow : EditorWindow {

        private EtDTileMap _tileMap;
        private EtDAutoTileTemplate _autoTileTemplate;

        private static readonly Vector2 AutoTileSetEditorIndent = new Vector2(4, 6);
        private static readonly Vector2 AutoTileSetEditorSize = new Vector2(400, 400);
        private readonly Vector2 _tileSize = new Vector2(100, 100);
        private readonly Vector2 _smallTileSize = new Vector2(50, 50);

        private const float BigRectIndent = 5f;

        private Vector2 _tiles;
        private Vector2 _smallRectSize;
        private Vector2 _smallCenterCoords;

        private List<EtDAutoTilePairView> _autoTilePairViews;

        private List<EtDAutoTileTemplate> _templates; 
        private List<string> _templatesNames; 

        private int _currentAutoTilePair;

        private bool _wholeTiles;
        private int  _choosedTileNumber;

        private IntVector2 _cellSize;
        private IntVector2 _smallCellSize;

        public void Init(EtDTileMap tileMap, EtDAutoTileTemplate autoTileTemplate) {
            _tileMap = tileMap;
            _autoTileTemplate = autoTileTemplate;
            _tiles = new Vector2(AutoTileSetEditorSize.x / _tileSize.x, AutoTileSetEditorSize.x / _tileSize.y);
            _smallRectSize = new Vector2(_smallTileSize.x - BigRectIndent * 2, _smallTileSize.y - BigRectIndent * 2);
            _smallCenterCoords = new Vector2(_smallTileSize.x / 2 - _smallRectSize.x / 2, _smallTileSize.y / 2 - _smallRectSize.y / 2);

            _currentAutoTilePair = 0;

            _autoTilePairViews = new List<EtDAutoTilePairView>();

           foreach (var autoTilePair in autoTileTemplate.AutoTilePairs) {
                _autoTilePairViews.Add(new EtDAutoTilePairView(this, autoTileTemplate, autoTilePair, true));
            }

           _templates = _tileMap.FindAllTemplates();

           _templatesNames = new List<string>();

            foreach (var template in _templates) {
                _templatesNames.Add(template.Name);
            }

            _cellSize = new IntVector2(_tileMap.CellSize);
            _smallCellSize = _cellSize / 2;
        }


        private void OnGUI() {

            if (_autoTilePairViews.Count > 0) {

                var autoTilePair = _autoTilePairViews[_currentAutoTilePair];
                if(autoTilePair == null) return;      

                var autoTilePairNames = _autoTilePairViews.Select(pair => pair.Name).ToArray();

                GUILayout.BeginVertical();

                _currentAutoTilePair = EditorGUILayout.Popup(_currentAutoTilePair, autoTilePairNames);

                HandleInput(autoTilePair.Draw(), autoTilePair);

                GUILayout.EndVertical();

            }

            EtDGUIUtils.HorizontalDiviner(3);

            if (EtDGUIUtils.CreateButton()) {
                _autoTilePairViews.Add(new EtDAutoTilePairView(this, _autoTileTemplate, new EtDAutoTilePair(_tileMap, _autoTileTemplate, _tileMap.CellSize, _tileMap.PixelToUnit)));
            }
        }



        private void HandleInput(Rect area, EtDAutoTilePairView pairView) {
            bool isSmall = false;

            Event e = Event.current;

            if(!area.Contains(e.mousePosition)) return;

            IEnumerable<Rect> smallRects = CalculateSmallRects(area);

            foreach (var rect in smallRects) {
                if (rect.Contains(e.mousePosition)) {
                    EtDDrawUtils.DrawSolidRectangleWithOutline(rect, EtDDrawUtils.AddTileFillColor, Color.clear);
                    isSmall = true;
                    break;
                }
            }

            if (!isSmall) {
                var bigRects = CalculateBigRects(area);

                foreach (var rect in bigRects) {
                    if (rect.Contains(e.mousePosition)) {
                        EtDDrawUtils.DrawSolidRectangleWithOutline(rect, EtDDrawUtils.AddTileFillColor, Color.clear);
                        break;
                    }
                }
            }            

            if (e.type == EventType.MouseDown) {
                var tileSize = new IntVector2(_tileMap.CellSize);

                int currentRectIndex = 0;

                _wholeTiles = true;

                if (isSmall) {
                    tileSize /= 2;
                    _wholeTiles = false;

                    foreach (var rect in smallRects)
                    {
                        if (rect.Contains(e.mousePosition)) {
                            _choosedTileNumber = currentRectIndex;
                            break;
                        }
                        currentRectIndex++;
                    }

                } else {

                    currentRectIndex = 0;
                    var bigRects = CalculateBigRects(area);

                    foreach (var rect in bigRects) {
                        if (rect.Contains(e.mousePosition)) {
                            _choosedTileNumber = currentRectIndex;
                            break;
                        }
                        currentRectIndex++;
                    }
                    
                }

                (GetWindow<EtDTileChooserWindow>()).Init(tileSize, _tileMap, ChangeTilesHandler);
            }

        }

        private void ChangeTilesHandler(IntVector2 pos, EtDTileSet tileSet) {

            var autoTilePair = _autoTilePairViews[_currentAutoTilePair];
            if(autoTilePair == null) return;    

            if (_wholeTiles) {
                autoTilePair.Pair.ChangeTileMaterial(_choosedTileNumber, tileSet.Material, pos);
            } else {

                int tileNumber = _choosedTileNumber / 4;
                int cornerNumber = _choosedTileNumber % 4;

                autoTilePair.Pair.ChangeTileMaterial(tileNumber, cornerNumber, tileSet.Material, pos);
            }
        }

        private IEnumerable<Rect> CalculateSmallRects(Rect area) {
            var rects = new List<Rect>();

            for (int y = 0; y < _tiles.y; y++) {
                for (int x = 0; x < _tiles.x; x++) {

                    rects.Add(new Rect(area.x + x * _tileSize.x + _smallCenterCoords.x,
                                       area.y + y * _tileSize.y + _smallCenterCoords.y,
                                      _smallRectSize.x, _smallRectSize.y));

                    rects.Add(new Rect(area.x + x * _tileSize.x + _smallTileSize.x + _smallCenterCoords.x,
                                       area.y + y * _tileSize.y + _smallCenterCoords.y,
                                       _smallRectSize.x, _smallRectSize.y));

                   rects.Add(new Rect(area.x + x * _tileSize.x + _smallCenterCoords.x,
                                      area.y + y * _tileSize.y + _smallTileSize.y + _smallCenterCoords.y,
                                       _smallRectSize.x, _smallRectSize.y));

                   rects.Add(new Rect(area.x + x * _tileSize.x + _smallTileSize.x + _smallCenterCoords.x,
                                      area.y + y * _tileSize.y + _smallTileSize.y + _smallCenterCoords.y,
                                       _smallRectSize.x, _smallRectSize.y));


                }
            }

            return rects;
        }

        private IEnumerable<Rect> CalculateBigRects(Rect area) {
            var rects = new List<Rect>();

            for (int y = 0; y < _tiles.y; y++)
            {
                for (int x = 0; x < _tiles.x; x++)
                {
                    rects.Add(new Rect(area.x + x * _tileSize.x,
                                       area.y + y * _tileSize.y,
                                       _tileSize.x, _tileSize.y));
                }
            }

            return rects;
        } 

        private void Update() {
            Repaint();
        }


        private class EtDAutoTilePairView {

            private EtDAutoTileEditorWindow _parent;
            
            private EtDAutoTileTemplate _tileTemplate;
            private EtDAutoTilePair _pair;

            private bool _exists;
            private bool _saveError = true;

            private int _currentConditionIndex;

            public string Name {
                get { return _pair.Name; }
                set { _pair.Name = value; }
            }

            public EtDAutoTileTemplate Template
            {
                get { return _tileTemplate; }
            }

            public EtDAutoTilePair Pair
            {
                get { return _pair; }
            }

            public EtDAutoTilePairView(EtDAutoTileEditorWindow parent, EtDAutoTileTemplate tileTemplate, EtDAutoTilePair pair, bool exists = false) {
                _parent = parent;
                _tileTemplate = tileTemplate;
                _pair = pair;
                _exists = exists;

                _currentConditionIndex = 0;

                _saveError = !_exists;

                if (_pair == null) {
                    throw new NullReferenceException("_pair is null");
                }
            }


            private void Save() {

                if (_tileTemplate.AutoTilePairs.Exists(any => any == _pair) && _exists) {
                    _saveError = true;
                    return;
                }

                if (_pair.Name.Length == 0) {
                    _saveError = true;
                    return;
                }

                var anotherTemplate = _pair.GetAnotherTemplate(_tileTemplate);

                _tileTemplate.AutoTilePairs.Add(_pair);
                anotherTemplate.AutoTilePairs.Add(_pair);

                _parent._tileMap.AddAutoTilePair(_pair);

                _saveError = false;
                _exists = true;

            }


            public Rect Draw() { 
         
                //Save errors
                if (!_exists) {
                    EtDGUIUtils.Box(EtDEditorDictionary.GetWord(EtDEditorDictionary.JustTemplate), Color.gray);
                }

                if (_saveError) {
                    EtDGUIUtils.Box(EtDEditorDictionary.GetWord(EtDEditorDictionary.Error) + "!", Color.red);
                    EtDGUIUtils.Box(EtDEditorDictionary.GetWord(EtDEditorDictionary.ExistsError), EtDGUIUtils.ExtraColor.Orange);

                    if (_pair != null) {

                        if (_pair.Name.Length == 0) {
                            EtDGUIUtils.Box(
                                            EtDEditorDictionary.GetPhrase(EtDEditorDictionary.FieldIsEmpty,
                                                EtDEditorDictionary.Name),
                                EtDGUIUtils.ExtraColor.Orange);
                        }

                    }
                }

                // Fields

                Name = EditorGUILayout.TextField(EtDEditorDictionary.GetWord(EtDEditorDictionary.Name), Name);

                Vector2 tileSize = _parent._tileSize;

                if (!_exists) {
                    _currentConditionIndex = EditorGUILayout.Popup(_currentConditionIndex, _parent._templatesNames.ToArray());
                    var currentConditionTemplate = _parent._templates[_currentConditionIndex];

                    EtDGUIUtils.Box(EtDEditorDictionary.GetWord(EtDEditorDictionary.Condition), Color.yellow);
                    GUILayout.Box("", GUILayout.Width(tileSize.x), GUILayout.Height(tileSize.y));

                    if (currentConditionTemplate != null) {
                        UnityEngine.GUI.DrawTextureWithTexCoords(GUILayoutUtility.GetLastRect(), currentConditionTemplate.Texture, currentConditionTemplate.TextureCoords);
                    }

                    if (_pair != null) {
                        _pair.Second = currentConditionTemplate;
                    }

                } else {
                    if (_pair != null) {

                        GUILayout.BeginHorizontal();

                        // Main template
                        GUILayout.BeginVertical();
                            EtDGUIUtils.Box("0", Color.yellow);
                            GUILayout.Box("", GUILayout.Width(tileSize.x), GUILayout.Height(tileSize.y));

                            UnityEngine.GUI.DrawTextureWithTexCoords(GUILayoutUtility.GetLastRect(), _pair.First.Texture, _pair.First.TextureCoords);
                        GUILayout.EndVertical();

                        // Second template
                        GUILayout.BeginVertical();
                            EtDGUIUtils.Box("1", Color.yellow);
                            GUILayout.Box("", GUILayout.Width(tileSize.x), GUILayout.Height(tileSize.y));

                            UnityEngine.GUI.DrawTextureWithTexCoords(GUILayoutUtility.GetLastRect(), _pair.Second.Texture, _pair.Second.TextureCoords);
                        GUILayout.EndVertical();


                        GUILayout.EndHorizontal();
                    }
                }


                // Tileset template
                Texture texture = EtDEditorSkin.Instance.FindTexture("Tiles");

                var viewRect = GUILayoutUtility.GetRect(texture.width, texture.height);
                viewRect.Set(viewRect.x, viewRect.y, texture.width, texture.height);

                int currentTileIndex = 0;

                //Debug.Log(_);

                for (int y = 0; y < _parent._tiles.y; y++) {
                    for (int x = 0; x < _parent._tiles.x; x++) {
                        //if(_pair == null) break;
                        EtDTileContainer tileContainer = _pair.Tiles[currentTileIndex];

                        //Debug.Log(tileContainer);

                        DrawPartOfTile(new Vector2(x * _parent._tileSize.x + viewRect.x + AutoTileSetEditorIndent.x,
                                                   y * _parent._tileSize.y + viewRect.y + AutoTileSetEditorIndent.y), tileContainer.LeftTopCorner);

                        DrawPartOfTile(new Vector2(x * _parent._tileSize.x + _parent._smallTileSize.x + viewRect.x + AutoTileSetEditorIndent.x,
                                                   y * _parent._tileSize.y + viewRect.y + AutoTileSetEditorIndent.y), tileContainer.RightTopCorner);

                        DrawPartOfTile(new Vector2(x * _parent._tileSize.x + viewRect.x + +AutoTileSetEditorIndent.x,
                                                   y * _parent._tileSize.y + _parent._smallTileSize.y + viewRect.y + AutoTileSetEditorIndent.y), tileContainer.LeftBottomCorner);

                        DrawPartOfTile(new Vector2(x * _parent._tileSize.x + _parent._smallTileSize.x + viewRect.x + AutoTileSetEditorIndent.x,
                                                   y * _parent._tileSize.y + _parent._smallTileSize.y + viewRect.y + AutoTileSetEditorIndent.y), tileContainer.RightBottomCorner);

                        currentTileIndex++;
                    }
                }

                UnityEngine.GUI.DrawTexture(viewRect, texture, ScaleMode.StretchToFill, true);

                if (!_exists) {
                    if (EtDGUIUtils.Button(EtDEditorDictionary.GetWord(EtDEditorDictionary.SaveCommand), Color.magenta))
                    {
                        Save();
                    }
                }


                return new Rect(viewRect.x + AutoTileSetEditorIndent.x,
                                             viewRect.y + AutoTileSetEditorIndent.y,
                                             AutoTileSetEditorSize.x,
                                             AutoTileSetEditorSize.y);


            }

            private void DrawPartOfTile(Vector2 position, EtDTile tile) {

                if (tile.Material == null) return; 

                UnityEngine.GUI.DrawTextureWithTexCoords(new Rect(position.x, position.y, _parent._smallTileSize.x, _parent._smallTileSize.y), 
                                                         tile.Material.mainTexture,
                                                         EtDMeshUtils.TextureCoords(tile.Material.mainTexture, tile.Position, _parent._smallCellSize ));
            }
        }

    }
}
