﻿using System;
using System.Collections.Generic;
using Assets.EtD.GUI;
using Assets.EtD.GUI.TileEditor;
using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;

namespace Assets.EtD.Editor {
    [CustomEditor(typeof(EtDTileMap))]
    public class EtDTileMapEditor : UnityEditor.Editor, ITileMapEditor {

        private EtDTileMap _tileMap;

        private EtDTileMaker _tileMaker;

        private Panels _currentPanel = Panels.Draw;

        private string[] _panelNames;
        private string[] PanelNames
        {
            get
            {
                if (_panelNames == null) {
                    _panelNames = new string[Enum.GetValues(typeof(Panels)).Length];

                    _panelNames[0] = EtDEditorDictionary.GetWord(EtDEditorDictionary.Draw);
                    _panelNames[1] = EtDEditorDictionary.GetWord(EtDEditorDictionary.Tilesets);
                    _panelNames[2] = EtDEditorDictionary.GetWord(EtDEditorDictionary.Settings);
                }

                return _panelNames;
            }
        }

        public EtDTileMap TileMap {
            get { return _tileMap;  }
        }

        private Dictionary<Panels, ISubEditor> _panelEditors;

        public Dictionary<Panels, ISubEditor> PanelEditors {
            get {

                if (_panelEditors == null) {
                    _panelEditors = new Dictionary<Panels, ISubEditor>();

                    _panelEditors[Panels.Draw] = new EtDTileMapEditorDraw(TileMap, this);
                    _panelEditors[Panels.TileSets] = new EtDTileMapEditorTileSets(TileMap, this);
                    _panelEditors[Panels.Settings] = new EtDTileMapEditorSettings(TileMap);
                }

                return _panelEditors;
            }
        }

         

        public enum Panels {
            Draw,
            TileSets,
            Settings
        }

        public void OnEnable() {
            _tileMap = (EtDTileMap) target;

            CreateTileMaker();
        }


        private void CreateTileMaker()
        {
            Transform t = _tileMap.transform.FindChild("TileMaker");
            if (t != null) DestroyImmediate(t.gameObject);

            Transform tileMakerTransform = EtDGameObjectUtils.AddChild(_tileMap.transform, "TileMaker");
            _tileMaker = tileMakerTransform.gameObject.AddComponent<EtDTileMaker>();
            _tileMaker.TileMap = TileMap;
        }

        public override void OnInspectorGUI() {

            // Edit-Save button
            EditSaveCommand();

            if(!TileMap.EditMode) return;

            EtDGUIUtils.HorizontalDiviner();

            // Editor panels
            _currentPanel = (Panels)GUILayout.Toolbar((int)_currentPanel, PanelNames);

            ISubEditor currentEditor = PanelEditors[_currentPanel];
            if (currentEditor != null) {
                currentEditor.OnInspectorGUI();
            }
            
        }

        private void EditSaveCommand() {
            Color editButtonColor = TileMap.EditMode ? Color.green : Color.red;

            string editButtonText = TileMap.EditMode ? EtDEditorDictionary.GetWord(EtDEditorDictionary.SaveCommand) :
                                                       EtDEditorDictionary.GetWord(EtDEditorDictionary.EditCommand);

            if (EtDGUIUtils.Button(editButtonText, editButtonColor)) {

                TileMap.EditMode = !TileMap.EditMode;
            } 
        }

        public void OnDisable() {
            if (_tileMaker != null) DestroyImmediate(_tileMaker.gameObject);
            _tileMaker = null;
        }


// ReSharper disable once InconsistentNaming
        public void OnSceneGUI() {
            ISubEditor currentEditor = PanelEditors[_currentPanel];
            if (currentEditor != null)
            {
                currentEditor.OnSceneGUI();
            }
        }

        public void ShowAutoTileEditorWindow(EtDTileMap tileMap, EtDAutoTileTemplate autoTileTemplate) {
            string editorHeader = EtDEditorDictionary.GetPhrase(EtDEditorDictionary.EditCommand, EtDEditorDictionary.AutoTiles);

            (EditorWindow.GetWindow<EtDAutoTileEditorWindow>(editorHeader)).Init(tileMap, autoTileTemplate);
        }

        public EtDTileMaker TileMaker
        {
            get { return _tileMaker; }
        }
    }
}
