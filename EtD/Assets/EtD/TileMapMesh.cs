﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEngine;

namespace Assets.EtD {
    class TileMapMesh : MonoBehaviour {

        [SerializeField]
        private MeshRenderer _meshRenderer;

        [SerializeField]
        private MeshFilter _meshFilter;

        public MeshRenderer MeshRenderer {
            get {
                return _meshRenderer ?? (_meshRenderer = gameObject.AddComponent<MeshRenderer>());
            }
        }

        public MeshFilter MeshFilter {
            get
            {
                return _meshFilter ?? (_meshFilter = gameObject.AddComponent<MeshFilter>());
            }
        }

        public Material Material
        {
            get { return MeshRenderer.sharedMaterial; }
        }

        public Mesh Mesh {
            get { return MeshFilter.sharedMesh ?? (MeshFilter.sharedMesh = new Mesh()); }
        }

        [SerializeField]
        private List<IntVector2> _positions;

        public List<IntVector2> Positions {
            get { return _positions ?? (_positions = new List<IntVector2>()); }
        }

        private Dictionary<IntVector2, int> _positionsMeshDictionary;

        public Dictionary<IntVector2, int> PositionsMeshDictionary {
            get {
                if (_positionsMeshDictionary == null) {
                    _positionsMeshDictionary = new Dictionary<IntVector2, int>();
                    for (int i = 0; i < Positions.Count; i++) {
                        _positionsMeshDictionary[Positions[i]] = i;
                    }
                }
                return _positionsMeshDictionary;
            }
        }

        public void AddTile(IntVector2 pos, Vector2[] textureCoords, Vector2 meshSize) {

            var meshPosition = new Vector2(pos.x * meshSize.x, pos.y * meshSize.y * -1);

            if (PositionsMeshDictionary.ContainsKey(pos)) {

                int index = PositionsMeshDictionary[pos];
                Vector2[] uv = Mesh.uv;

                uv[4 * index] = textureCoords[2];
                uv[4 * index + 1] = textureCoords[3];
                uv[4 * index + 2] = textureCoords[1];
                uv[4 * index + 3] = textureCoords[0];

                Mesh.uv = uv;

                Mesh.RecalculateNormals();

            } else {
                Positions.Add(pos);
                PositionsMeshDictionary[pos] = Positions.Count - 1;

                Vector3[] vertices = Mesh.vertices;
                int vertLength = vertices.Length;

                Array.Resize(ref vertices, vertices.Length + 4);

                vertices[vertLength] = new Vector3(meshPosition.x + meshSize.x, meshPosition.y - meshSize.y);
                vertices[vertLength + 1] = new Vector3(meshPosition.x, meshPosition.y - meshSize.y); 
                vertices[vertLength + 2] = new Vector3(meshSize.x + meshPosition.x, meshPosition.y);
                vertices[vertLength + 3] = new Vector3(meshPosition.x, meshPosition.y); 

                int[] triangles = Mesh.triangles;
                int triLength = triangles.Length;

                // 0, 3, 1
                // 0, 2, 3
                // На каждую грань (6 граней) по 2 треугольника, на каждый треугольник 3 вершины
                Array.Resize(ref triangles, triangles.Length + 6);

                triangles[triLength] = vertLength;
                triangles[triLength + 1] = vertLength + 3;
                triangles[triLength + 2] = vertLength + 1;

                triangles[triLength + 3] = vertLength;
                triangles[triLength + 4] = vertLength + 2;
                triangles[triLength + 5] = vertLength + 3;

                Vector2[] uv = Mesh.uv;
                int uvLenght = uv.Length;
                Array.Resize(ref uv, uv.Length + 4);

                uv[uvLenght] = textureCoords[2];
                uv[uvLenght + 1] = textureCoords[3];
                uv[uvLenght + 2] = textureCoords[1];
                uv[uvLenght + 3] = textureCoords[0];

                Mesh.vertices = vertices;
                Mesh.triangles = triangles;
                Mesh.uv = uv;

                Mesh.RecalculateNormals();
            }

        }
    }
}
