﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEngine;

namespace Assets.EtD
{
    public class TileContainer : MonoBehaviour {
        
        public class Corner {
            public const string LeftTop = "LeftTop";
            public const string RightTop = "RightTop";
            public const string LeftBottom = "LeftBottom";
            public const string RightBottom = "RightBottom"; 
        }

        private EtDTileMap _tileMap;

        public EtDTileMap TileMap {
            get { return _tileMap ?? (_tileMap = transform.root.GetComponent<EtDTileMap>()); }
        }

        public IntVector2 Position;
        public Material Material;

        public IntVector2 TileSize;
        public Vector2 MeshSize;

        private IntVector2 _smallPosition;

        public IntVector2 SmallPosition {
            get { return _smallPosition ?? (_smallPosition = new IntVector2(Position.x * 2, Position.y * 2)); }  
        }


        public EtDAutoTileTemplate AutoTileTemplate {
            get { return TileMap.FindTemplate(Position); }
        }

        private List<Tile> _corners;


        public List<Tile> Corners {
            get { return _corners ?? (_corners = new List<Tile>()); }
        }

        public Tile LeftTopCorner
        {
            get { return Corners[0]; }
        }

        public Tile RightTopCorner
        {
            get { return Corners[1]; }
        }

        public Tile LeftBottomCorner
        {
            get { return Corners[2]; }
        }

        public Tile RightBottomCorner {
            get { return Corners[3]; }
        }



        public void CreateTile(EtDTileMap tileMap, Material material, IntVector2 startPosition, IntVector2 tileSize, Vector2 meshSize) {
            _tileMap = tileMap;
            Material = material;
            Position = new IntVector2(startPosition);
            TileSize = new IntVector2(tileSize.x / 2, tileSize.y / 2);
            MeshSize = new Vector2(meshSize.x / 2, meshSize.y / 2);

            Corners.Clear();

            Transform leftTopCorner = EtDGameObjectUtils.AddChild(transform, Corner.LeftTop, true);
            var leftTopTile = leftTopCorner.gameObject.AddComponent<Tile>();
            Corners.Add(leftTopTile);

            Transform rightTopCorner = EtDGameObjectUtils.AddChild(transform, Corner.RightTop, true);

            Vector3 rightTopCornerPosition = rightTopCorner.transform.localPosition;
            rightTopCornerPosition.x += MeshSize.x;
            rightTopCorner.transform.localPosition = rightTopCornerPosition;

            var rightTopTile = rightTopCorner.gameObject.AddComponent<Tile>();
            Corners.Add(rightTopTile);

            Transform leftBottomCorner = EtDGameObjectUtils.AddChild(transform, Corner.LeftBottom, true);

            Vector3 leftBottomCornerPosition = leftBottomCorner.transform.localPosition;
            leftBottomCornerPosition.y -= MeshSize.y;
            leftBottomCorner.transform.localPosition = leftBottomCornerPosition;

            var leftBottomTile = leftBottomCorner.gameObject.AddComponent<Tile>();
            Corners.Add(leftBottomTile);

            Transform rightBottomCorner = EtDGameObjectUtils.AddChild(transform, Corner.RightBottom, true);

            Vector3 rightBottomCornerPosition = rightBottomCorner.transform.localPosition;
            rightBottomCornerPosition.x += MeshSize.x;
            rightBottomCornerPosition.y -= MeshSize.y;
            rightBottomCorner.transform.localPosition = rightBottomCornerPosition;

            var rightBottomTile = rightBottomCorner.gameObject.AddComponent<Tile>();
            Corners.Add(rightBottomTile);

            ChangeCornerMaterial(EtDTileContainer.Corner.LeftTop, Material);
            ChangeCornerMaterial(EtDTileContainer.Corner.RightTop, Material);
            ChangeCornerMaterial(EtDTileContainer.Corner.LeftBottom, Material);
            ChangeCornerMaterial(EtDTileContainer.Corner.RightBottom, Material);
        }

        public void ChangeCornerMaterial(int cornerNumber, Material material) {
            if(cornerNumber < 0 || cornerNumber >= 4) return;

            Corners[cornerNumber].CreateTile(material, CalculateCornerTexturePosition(cornerNumber, SmallPosition), TileSize, MeshSize);          
        }


        public IntVector2 CalculateCornerTexturePosition(int cornerNumber, IntVector2 position) {
            IntVector2 texturePosition = null;

            switch (cornerNumber) {
                case EtDTileContainer.Corner.LeftTop:
                    texturePosition = new IntVector2(position);
                    break;

                case EtDTileContainer.Corner.RightTop:
                    texturePosition = new IntVector2(position.x + 1, position.y);
                    break;

                case EtDTileContainer.Corner.LeftBottom:
                    texturePosition = new IntVector2(position.x, position.y + 1);
                    break;

                case EtDTileContainer.Corner.RightBottom:
                    texturePosition = new IntVector2(position.x + 1, position.y + 1);
                    break;
            }

            return texturePosition;
        }

    }
}
