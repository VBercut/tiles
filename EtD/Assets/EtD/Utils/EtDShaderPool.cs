﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.EtD.Utils
{
    class EtDShaderPool {

        public const string SpriteShader = "Sprites/Default";
        public const string DiffuseShader = "Sprites/Diffuse";

        public const string Specular = "Specular";
        public const string BumpedDiffuse = "Bumped Diffuse";
        public const string BumpedSpecular = "Bumped Specular";

        public const string ReflectiveVertexLit = "Reflective Vertex-Lit";

        private static List<string> _shadersList;

        public static string[] ShadersList
        {
            get {
                if (_shadersList == null)
                {
                    _shadersList = new List<string>
                    {
                        SpriteShader,
                        DiffuseShader,
                        Specular,
                        BumpedDiffuse,
                        BumpedSpecular,
                        ReflectiveVertexLit
                    };
                }

                return _shadersList.ToArray();
            }
        }

        private static Dictionary<string, Shader> _shaders;

        private static Dictionary<string, Shader> Shaders
        {
            get { return _shaders ?? (_shaders = new Dictionary<string, Shader>()); }
        }

        public static Shader FindShader(string name) {
            if (!Shaders.ContainsKey(name)) {
                Shader shader = Shader.Find(name);

                if(shader != null)
                    Shaders[name] = shader;
            }

            return Shaders[name];
        }


        public static Material CreateMaterial(string shaderName) {
            return new Material(FindShader(shaderName));
        }

        public static Material CreateMaterial(string shaderName, Texture mainTexture) {
            return new Material(FindShader(shaderName)) { mainTexture = mainTexture };
        }

    }
}
