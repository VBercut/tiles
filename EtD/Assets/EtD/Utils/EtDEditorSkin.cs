﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.EtD.Utils
{
    public class EtDEditorSkin
    {

        private static EtDEditorSkin _instance;
        private GUISkin _skin;
        private Dictionary<string, Texture> _textures;


        public static EtDEditorSkin Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new EtDEditorSkin { _skin = Resources.Load<GUISkin>("EtDEditor") };
                    Texture[] textures = Resources.LoadAll<Texture>("UI");

                    _instance._textures = new Dictionary<string, Texture>();

                    foreach (Texture currentTexture in textures) {
                        if (_instance._textures.ContainsKey(currentTexture.name)) continue;

                        _instance._textures[currentTexture.name] = currentTexture;
                    }
                }

                return _instance;
            }
        }

        public GUIStyle GetStyle(string style)
        {
            return _skin.GetStyle(style);
        }

        public Texture FindTexture(string name)
        {
            if (!_textures.ContainsKey(name)) return null;
            return _textures[name];
        }

        public bool DrawDeleteButton(float width, float height)
        {
            UnityEngine.GUI.contentColor = UnityEngine.GUI.color = Color.white;
           UnityEngine.GUI.backgroundColor = Color.red;

            bool buttonClicked = GUILayout.Button(FindTexture("22-skull-n-bones"),
                GUILayout.Width(width), GUILayout.Height(height));

            UnityEngine.GUI.backgroundColor = Color.white;

            return buttonClicked;
        }

        public bool DeleteButton
        {
            get
            {
                return DrawDeleteButton(28, 20);
            }
        }

    }
}
