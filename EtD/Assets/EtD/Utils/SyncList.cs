﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.EtD.Utils {

    [Serializable]
    class SyncList<T> : IList, IList<T> {

        private readonly List<T> _data;
        private readonly List<Action> _actions;

        private bool _locked;


        private enum Actions {
           Add,
           Insert,
           Remove,
           RemoveAt,
           Clear
        }

        private class Action {
            public T o;
            public Actions action;
            public int index;

            public Action(T o, Actions action, int index)
            {
                this.o = o;
                this.action = action;
                this.index = index;
            }

            public Action(Actions action, int index) {
                this.action = action;
                this.index = index;
            }

            public Action(Actions action) {
                this.action = action;
            }
        }


        public SyncList() {
            _data = new List<T>();
            _actions = new List<Action>();

            _locked = false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) _data).GetEnumerator();
        }

        public void CopyTo(Array array, int index)
        {
            ((ICollection) _data).CopyTo(array, index);
        }

        public bool Remove(T item) {
            if (_locked) {
                _actions.Add(new Action(item, Actions.Remove, -1));
                return false;
            }

            return _data.Remove(item);
        }

        int ICollection<T>.Count
        {
            get { return _data.Count; }
        }

        bool ICollection<T>.IsReadOnly
        {
            get { return ((IList)_data).IsReadOnly; }
        }


        int ICollection.Count
        {
            get { return _data.Count; }
        }

        public bool IsSynchronized
        {
            get { return ((ICollection) _data).IsSynchronized; }
        }

        public object SyncRoot
        {
            get { return ((ICollection) _data).SyncRoot; }
        }

        public int Add(object value) {
            return ((IList) _data).Add(value);
        }

        public void Add(T item) {
            if (_locked) {
                _actions.Add(new Action(item, Actions.Add, -1));
                return;
            }

            _data.Add(item);
        }

        void ICollection<T>.Clear() {

            if (_locked) {
                _actions.Add(new Action(Actions.Clear));
                return;
            }

            _data.Clear();
        }

        public bool Contains(T item)
        {
            return _data.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _data.CopyTo(array, arrayIndex);
        }

        void IList.Clear() {

            if (_locked) {
                _actions.Add(new Action(Actions.Clear));
                return;
            }

            ((IList) _data).Clear();
        }

        public bool Contains(object value)
        {
            return ((IList) _data).Contains(value);
        }

        public int IndexOf(object value)
        {
            return ((IList) _data).IndexOf(value);
        }

        public void Insert(int index, object value)
        {
            ((IList) _data).Insert(index, value);
        }

        public void Remove(object value)
        {
            ((IList) _data).Remove(value);
        }

        public int IndexOf(T item)
        {
            return _data.IndexOf(item);
        }

        public void Insert(int index, T item) {

            if (_locked) {
                _actions.Add(new Action(item, Actions.Insert, index));
                return;
            }

            _data.Insert(index, item);
        }

        void IList<T>.RemoveAt(int index) {

            if (_locked) {
                _actions.Add(new Action(Actions.RemoveAt, index));
                return;
            }

            _data.RemoveAt(index);
        }

        bool IList.IsReadOnly
        {
            get { return ((IList)_data).IsReadOnly; }
        }

        public T this[int index]
        {
            get { return _data[index]; }
            set { _data[index] = value; }
        }

        void IList.RemoveAt(int index) {

            if (_locked) {
                _actions.Add(new Action(Actions.RemoveAt, index));
                return;
            }

            ((IList) _data).RemoveAt(index);
        }

        public bool IsFixedSize
        {
            get { return ((IList) _data).IsFixedSize; }
        }

        object IList.this[int index]
        {
            get { return _data[index]; }
            set { _data[index] = (T) value; }
        }


        public void Lock() {
            _locked = true;
        }

        public void Unlock() {
            if(!_locked) return;

            foreach (var action in _actions) {
                DoAction(action);
            }

            _actions.Clear();
            _locked = false;
        }

        private void DoAction(Action action) {
            switch (action.action) {
                case Actions.Add:
                    _data.Add(action.o);
                break;

                case Actions.Insert:
                    _data.Insert(action.index, action.o);
                break;

                case Actions.Remove:
                    _data.Remove(action.o);
                break;

                case Actions.RemoveAt:
                    _data.RemoveAt(action.index);
                break;

                case Actions.Clear:
                    _data.Clear();
                break;
            }
        }

    }
}
