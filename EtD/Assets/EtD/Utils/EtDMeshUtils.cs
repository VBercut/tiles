﻿using System.Text;
using Assets.EtD.Math;
using UnityEngine;

namespace Assets.EtD.Utils
{
    public class EtDMeshUtils {


        public static Mesh BuildRectMesh(Vector2 meshSize) {

            //Build squad

            // 0 ----- 1
            // |       |
            // |       |
            // 2 ----- 3

            var mesh = new Mesh();

            var vertices = new Vector3[4];
            int[] triangles = { 0, 1, 2, 3, 0, 2 };
            var uv = new Vector2[4];

            vertices[0] = new Vector3(0, 0);
            vertices[1] = new Vector3(meshSize.x, 0);
            vertices[2] = new Vector3(meshSize.x, -meshSize.y);
            vertices[3] = new Vector3(0, -meshSize.y);

            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.uv = uv;

            mesh.RecalculateNormals();

            return mesh;
        }

        public static Mesh BuildRectMesh(Vector2 meshSize, Material material, IntVector2 position, IntVector2 tileSize) {
            var mesh = BuildRectMesh(meshSize);

            int maxTileSetY = MaxTileSetY(material.mainTexture, tileSize);
            var textureCoords = new IntVector2(position.x, System.Math.Abs(maxTileSetY - position.y));

            ApplyTextureOnRectMesh(mesh, new Rect(textureCoords.x, textureCoords.y, tileSize.x, tileSize.y), material);

            return mesh;
        }
        public static int MaxTileSetY(Texture texture, IntVector2 tileSize)
        {
            return (int) (texture.height / tileSize.y);
        }

        public static Rect TextureCoords(Texture texture, IntVector2 position, IntVector2 tileSize) {
            int maxTileSetY = MaxTileSetY(texture, tileSize);
            var textureCoords = new IntVector2(position.x, System.Math.Abs(maxTileSetY - position.y) - 1);

            var textels = new Vector2((float)tileSize.x / texture.width, (float)tileSize.y / texture.height);

            return new Rect(textureCoords.x * textels.x, textureCoords.y * textels.y, textels.x, textels.y);
        }

        public static Vector2[] TextureCoordsUV(Texture texture, IntVector2 position, IntVector2 tileSize) {
            var uv = new Vector2[4];
            int maxTileSetY = MaxTileSetY(texture, tileSize);
            var area = new Vector2(position.x, System.Math.Abs(maxTileSetY - position.y));
            var tile = new Vector2(tileSize.x, tileSize.y);

            var textels = new Vector2(tile.x / texture.width, tile.y / texture.height);

            uv[0] = new Vector2(area.x * textels.x, area.y * textels.y);
            uv[1] = new Vector2(area.x * textels.x + textels.x, area.y * textels.y);
            uv[2] = new Vector2(area.x * textels.x + textels.x, area.y * textels.y - textels.y);
            uv[3] = new Vector2(area.x * textels.x, area.y * textels.y - textels.y);

            return uv;
        }


        public static Mesh ApplyTextureOnRectMesh(Mesh mesh, Material material) {
            Texture texture = material.mainTexture;
            return ApplyTextureOnRectMesh(mesh, new Rect(0, 0, texture.width, texture.height), material);
        }


        public static Mesh ApplyTextureOnRectMesh(Mesh mesh, Rect area, Material material) {
            Texture texture = material.mainTexture;

            var textels = new Vector2(area.width / texture.width, area.height / texture.height);

            var uv = new Vector2[4];

            uv[0] = new Vector2(area.x * textels.x, area.y * textels.y);
            uv[1] = new Vector2(area.x * textels.x + textels.x, area.y * textels.y);
            uv[2] = new Vector2(area.x * textels.x + textels.x, area.y * textels.y - textels.y);
            uv[3] = new Vector2(area.x * textels.x, area.y * textels.y - textels.y);

            mesh.uv = uv;
            
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            return mesh;
        }


        public static Mesh BuildGridMesh(Material material, Rect textureArea, Vector2 textureTileSize, Vector2 direction, Vector2 meshTileSize, IntVector2 tiles) {
            Texture texture = material.mainTexture;

            var mesh = new Mesh();

            var vertices = new Vector3[4 * tiles.x * tiles.y];
            var triangles = new int[6 * tiles.x * tiles.y];
            var uv = new Vector2[4 * tiles.x * tiles.y];

            var textels = new Vector2(textureTileSize.x / texture.width, textureTileSize.y / texture.height);

            var position = new IntVector2((int)textureArea.x, (int)textureArea.y);

            int currentVertex = 0;
            int currentTriangle = 0;

            for (int y = 0; y < tiles.y; y++)
            {
                for (int x = 0; x < tiles.x; x++)
                {
                    var meshPosition = new Vector2(meshTileSize.x*x*direction.x, meshTileSize.y*-y*direction.y);

                    vertices[currentVertex] = new Vector3(meshPosition.x, meshPosition.y);
                    vertices[currentVertex + 1] = new Vector3(meshPosition.x + meshTileSize.x, meshPosition.y);
                    vertices[currentVertex + 2] = new Vector3(meshPosition.x + meshTileSize.x,
                        meshPosition.y - meshTileSize.y);
                    vertices[currentVertex + 3] = new Vector3(meshPosition.x, meshPosition.y - meshTileSize.y);

                    triangles[currentTriangle] = currentVertex;
                    triangles[currentTriangle + 1] = currentVertex + 1;
                    triangles[currentTriangle + 2] = currentVertex + 2;
                    triangles[currentTriangle + 3] = currentVertex + 3;
                    triangles[currentTriangle + 4] = currentVertex;
                    triangles[currentTriangle + 5] = currentVertex + 2;


                    uv[currentVertex] = new Vector2(position.x*textels.x, position.y*textels.y);
                    uv[currentVertex + 1] = new Vector2(position.x*textels.x + textels.x, position.y*textels.y);
                    uv[currentVertex + 2] = new Vector2(position.x*textels.x + textels.x,
                        position.y*textels.y - textels.y);
                    uv[currentVertex + 3] = new Vector2(position.x*textels.x, position.y*textels.y - textels.y);

                    position.x += 1;

                    if (position.x >= textureArea.x + tiles.x)
                    {
                        position.x = (int) textureArea.x;
                    }

                    currentVertex += 4;
                    currentTriangle += 6;
                }

                position.x = (int) textureArea.x;
                position.y -= 1;

                if (position.y <= textureArea.y - tiles.y)
                {
                    position.y = (int) textureArea.y;
                }

            }

            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.uv = uv;

            mesh.RecalculateNormals();


            return mesh;
        }


        public static Vector2 CalculateSizeForRectMesh(float pixelToUnit, Vector2 size) {
            return size / pixelToUnit;
        }

    }

}
