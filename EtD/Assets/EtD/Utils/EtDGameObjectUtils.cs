﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.EtD.Utils
{
    public class EtDGameObjectUtils {

        public static Transform AddChild(Transform parent, String name, bool unique = false) {
            if (unique) {
                Transform child = parent.FindChild(name);
                if(child) Object.DestroyImmediate(child.gameObject);
            }

            return AddChild(parent, name, Vector3.zero, Vector3.zero); 
        }


        public static Transform AddChild(Transform parent, String name, Vector3 position, Vector3 rotation) {
            Transform child = new GameObject(name).transform;
            child.parent = parent;
            child.localPosition = position;
            child.localScale = Vector3.one;
            Quaternion quaternion = child.transform.localRotation;
            quaternion.eulerAngles = rotation;
            child.transform.localRotation = quaternion;

            return child;
        }

        public static Transform CreateObject(String name) {
            return CreateObject(name, Vector3.zero, Vector3.zero);
        }


        public static Transform CreateObject(String name, Vector3 position, Vector3 rotation) {
            Transform obj = new GameObject(name).transform;
            obj.localPosition = position;
            obj.localScale = Vector3.one;
            Quaternion quaternion = obj.transform.localRotation;
            quaternion.eulerAngles = rotation;
            obj.transform.localRotation = quaternion;

            return obj;
        }


        public static void RemoveChild(Transform parent, String name) {
            Transform child = parent.FindChild(name);
            if(child == null) return;

            Object.DestroyImmediate(child.gameObject);
        }
    }
}
