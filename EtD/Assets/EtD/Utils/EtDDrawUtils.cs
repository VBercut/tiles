﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Assets.EtD.Utils {
    public class EtDDrawUtils {

        public static readonly Color BlackBorderColor = new Color(0, 0, 0, 1);
        public static readonly Color WhiteBorderColor = new Color(1, 1, 1, 0.9f);
        public static readonly Color BlackFillColor = new Color(0, 0, 0, 0.2f);


        public static readonly Color SelfTileFillColor = new Color(0.9f, 0, 0, 0.2f);
        public static readonly Color OtherTileFillColor = new Color(0, 0, 0.9f, 0.2f);

        public static readonly Color AddTileFillColor = new Color(0.33f, 0.89f, 0.33f, 0.33f);
        public static readonly Color EraseTileFillColor = new Color(0.89f, 0.43f, 0.33f, 0.66f);


        public static void DrawSolidRectangleWithOutline(Rect rect) {
            DrawSolidRectangleWithOutline(rect, BlackFillColor, BlackBorderColor);
        }

        public static void DrawSolidRectangleWithOutline(Rect rect, Color fillColor, Color borderColor) {
            Vector3[] rectVerts = {
                    new Vector3(rect.x, rect.y, 0),
                    new Vector3(rect.x + rect.width, rect.y, 0),
                    new Vector3(rect.x + rect.width, rect.y + rect.height, 0),
                    new Vector3(rect.x, rect.y + rect.height, 0)
                };

            Handles.DrawSolidRectangleWithOutline(rectVerts, fillColor, borderColor);
        }


        public static void DrawSelfTile(Rect rect) {
            DrawSolidRectangleWithOutline(rect, SelfTileFillColor, BlackBorderColor);
        }

        public static void DrawOtherTile(Rect rect) {
            DrawSolidRectangleWithOutline(rect, OtherTileFillColor, BlackBorderColor);
        }

        public static void DrawEraseTileTool(Rect rect) {
            DrawSolidRectangleWithOutline(rect, EraseTileFillColor, WhiteBorderColor);
        }

        public static void DrawAddTileTool(Rect rect) {
            DrawSolidRectangleWithOutline(rect, AddTileFillColor, WhiteBorderColor);
        }
    }
}
