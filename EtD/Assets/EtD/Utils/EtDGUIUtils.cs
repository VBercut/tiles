﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.EtD.Utils {
// ReSharper disable once InconsistentNaming
    public class EtDGUIUtils {

        private static Color _defaultColor = UnityEngine.GUI.color;

        public static class ExtraColor {
            public static readonly Color Orange = new Color(255, 102, 0);
            public static readonly Color ExtraOrange = new Color(255, 153, 0);
            public static readonly Color Black = new Color(66, 66, 66);
            public static readonly Color White = new Color(233, 233, 233);
            public static readonly Color Gray = new Color(188, 188, 188);
            public static readonly Color Blue = new Color(50, 153, 187);
        }

        public static bool Button(string text, Color color) {
            PushColor(color);
            bool pushed = GUILayout.Button(text);
            PopColor();

            return pushed;
        }

        public static bool Button(string text, Color color, float width) {
            return Button(text, color, GUILayout.Width(width));
        }


        public static bool Button(string text, Color color, bool fromDictionary) {
            PushColor(color);
            bool pushed = GUILayout.Button(GetText(text, fromDictionary));
            PopColor();

            return pushed;
        }

        public static bool Button(string text, Color color, float width, bool fromDictionary){
            return Button(GetText(text, fromDictionary), color, GUILayout.Width(width));
        }

        public static bool Button(string text, Color color, params GUILayoutOption[] options) {
            PushColor(color);
            bool pushed = GUILayout.Button(text, options);
            PopColor();

            return pushed; 
        }

        private static string GetText(string text, bool fromDictionary) {
            return fromDictionary ? EtDEditorDictionary.GetWord(text) : text;
        }

        public static void HorizontalDiviner() {
            HorizontalDiviner(UnityEngine.GUI.color, 1);
        }

        public static void HorizontalDiviner(Color color) {
            HorizontalDiviner(color, 1);
        }

        public static void HorizontalDiviner(float height) {
            HorizontalDiviner(UnityEngine.GUI.color, height);
        }


        public static void HorizontalDiviner(Color color, float height)
        {
            PushColor(color);
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(height));
            PopColor();
        }


        private static void PushColor(Color color) {
            _defaultColor = UnityEngine.GUI.color;
            UnityEngine.GUI.color = color;
        }

        private static void PopColor() {
            UnityEngine.GUI.color = _defaultColor; 
        }


        public static bool MyFocusControl(Texture2D texture, params GUILayoutOption[] options)
        {
            int id = GUIUtility.GetControlID(FocusType.Passive);
            Color color = UnityEngine.GUI.color;

            UnityEngine.GUI.color = GUIUtility.hotControl == id ? Color.green : Color.red;
            Rect rect = GUILayoutUtility.GetRect(texture.width, texture.height, options);

            switch (Event.current.type)
            {
                case EventType.MouseDown:
                    if (rect.Contains(Event.current.mousePosition))
                    {
                        GUIUtility.hotControl = id;
                        Event.current.Use();
                    }
                    break;
                case EventType.MouseUp:
                    if (GUIUtility.hotControl == id)
                    {
                        GUIUtility.hotControl = 0;
                        Event.current.Use();
                    }
                    break;
                case EventType.Repaint:
                    UnityEngine.GUI.DrawTexture(rect, texture);
                    break;
            }

            UnityEngine.GUI.color = color;

            return GUIUtility.hotControl == id;
        }

        public static void TextField(string text, Color color) {
            PushColor(color);
            EditorGUILayout.TextField(text);
            PopColor();
        }


        public static string TextField(string text) {
            return EditorGUILayout.TextField(text);
        }


        public static void Label(string text, Color color) {
            PushColor(color);
            GUILayout.Label(text);
            PopColor();
        }

        public static void Label(string text) {
            GUILayout.Label(text);
        }

        private static Dictionary<string, Rect> _rects;

        private static Dictionary<string, Rect> Rects
        {
            get { return _rects ?? (_rects = new Dictionary<string, Rect>()); }
        } 


        public static Rect GetRect(string element, float width, float height) {
            Rect newRect = GUILayoutUtility.GetRect(width, height);

            if (!Rects.ContainsKey(element) || newRect.width > 1 || newRect.height > 1) {
                Rects[element] = newRect;
            }

            return Rects[element];
        }

        public static bool ShowDialog(string message) {
            return EditorUtility.DisplayDialog(message, // What are we do
                EtDEditorDictionary.GetWord(EtDEditorDictionary.Really) + "?", // Really!?!?!?
                EtDEditorDictionary.GetWord(EtDEditorDictionary.NoCommand), // Cancel
                EtDEditorDictionary.GetWord(EtDEditorDictionary.YesCommand));
        }


        public static bool CreateButton() {
            return Button(EtDEditorDictionary.GetWord(EtDEditorDictionary.CreateCommand), Color.yellow);
        }

        public static void Box(string text, Color color) {
            PushColor(color);
            GUILayout.Box(text);
            PopColor();
        }
    }
}