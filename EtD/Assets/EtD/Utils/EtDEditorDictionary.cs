﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.EtD.Utils
{
    public class EtDEditorDictionary {

        private static Dictionary<SystemLanguage, Dictionary<string, string>> _languageDictionary;

        private static SystemLanguage? _currentLanguage;

        public static SystemLanguage? CurrentLanguage
        {
            get { return _currentLanguage; }
            set { _currentLanguage = value; }
        }

        private const string EmptyString    = "";

        public const string  SaveCommand    = "Save";
        public const string  EditCommand    = "Edit";
        public const string  AddCommand     = "Add";
        public const string  DeleteCommand  = "Delete";
        public const string  CreateCommand  = "Create";
        public const string  ShowCommand    = "Show";
        public const string  HideCommand    = "Hide";

        public const string  YesCommand     = "Yes";
        public const string  NoCommand      = "No";

        public const string  WarningCommand = "Warning";

        public const string  Error          = "Error";

        public const string  Really         = "Really";

        public const string  Layers         = "Layers";
        public const string  Layer          = "Layer";
        public const string  Pallete        = "Pallete";

        public const string  NoMaterials    = "NoMaterials";

        public const string  Tileset        = "Tileset";

        public const string  Draw           = "Draw";
        public const string  Tilesets       = "Tilesets";
        public const string  Settings       = "Settings";

        public const string  PalleteScale   = "PalleteScale";
        public const string  GridColor      = "GridColor";
        public const string  ShowGrid       = "ShowGrid";

        public const string  Shaders        = "Shaders";

        public const string  SelectTileSet  = "SelectTileSet";
        public const string  NoMainTexture  = "NoMainTexture";

        public const string  AutoTiles      = "AutoTiles";

        public const string  NewTemplate    = "NewTemplate";
     
        public const string  Name           = "Name";

        public const string  JustTemplate   = "JustTemplate";

        public const string  ExistsError    = "ExistsError";

        public const string  FieldIsEmpty    = "FieldIsEmpty";

        public const string  Condition       = "Condition";
        public const string  Size            = "Size";

        public static Dictionary<SystemLanguage, Dictionary<string, string>> LanguageDictionary {
            get {
                if (_languageDictionary == null) {
                   _languageDictionary = new Dictionary<SystemLanguage, Dictionary<string, string>>();
 
                   // English
                   AddWord(SystemLanguage.English, EditCommand, "Edit");
                   AddWord(SystemLanguage.English, SaveCommand, "Save");
                   AddWord(SystemLanguage.English, AddCommand, "Add");
                   AddWord(SystemLanguage.English, DeleteCommand, "Delete");
                   AddWord(SystemLanguage.English, CreateCommand, "Create");
                   AddWord(SystemLanguage.English, ShowCommand, "Show");
                   AddWord(SystemLanguage.English, HideCommand, "Hide");

                   AddWord(SystemLanguage.English, Layers, "Layers");
                   AddWord(SystemLanguage.English, Layer, "Layer");
                   AddWord(SystemLanguage.English, Pallete, "Pallete");

                   AddWord(SystemLanguage.English, YesCommand, "Yes");
                   AddWord(SystemLanguage.English, NoCommand, "No");

                   AddWord(SystemLanguage.English, Really, "Really");

                   AddWord(SystemLanguage.English, Error, "Error");

                   AddWord(SystemLanguage.English, NoMaterials, "There is no material.\n" +
                                                                "You can add some materials in «TileSets» tab.");

                   AddWord(SystemLanguage.English, Tileset, "Tileset");
                   AddWord(SystemLanguage.English, AutoTiles, "AutoTiles");

                   AddWord(SystemLanguage.English, Draw, "Draw");
                   AddWord(SystemLanguage.English, Tilesets, "Tilesets");
                   AddWord(SystemLanguage.English, Settings, "Settings");

                   AddWord(SystemLanguage.English, PalleteScale, "Pallete scale");

                   AddWord(SystemLanguage.English, Shaders, "Shaders");

                   AddWord(SystemLanguage.English, GridColor, "Grid color");
                   AddWord(SystemLanguage.English, ShowGrid, "Show grid");

                   AddWord(SystemLanguage.English, SelectTileSet, "Select any tileset to edit");

                   AddWord(SystemLanguage.English, NoMainTexture, "Current tileSet has no texture, add it and try again.");

                   AddWord(SystemLanguage.English, NewTemplate, "New template");

                   AddWord(SystemLanguage.English, Condition, "Condition");

                   AddWord(SystemLanguage.English, Name, "Name");
                   AddWord(SystemLanguage.English, FieldIsEmpty, "Field is empty");

                   AddWord(SystemLanguage.English, ExistsError, "Exactly the same object already exists.\n" +
                                                                 "Change something and try again.");

                   AddWord(SystemLanguage.English, JustTemplate, "It just a template.\n" +
                                                                 "You should save it.");

                   AddWord(SystemLanguage.English, Size, "Size");

                   // Russian
                   AddWord(SystemLanguage.Russian, EditCommand, "Редактировать");
                   AddWord(SystemLanguage.Russian, SaveCommand, "Сохранить");
                   AddWord(SystemLanguage.Russian, AddCommand, "Добавить");
                   AddWord(SystemLanguage.Russian, DeleteCommand, "Удалить");
                   AddWord(SystemLanguage.Russian, CreateCommand, "Создать");
                   AddWord(SystemLanguage.Russian, ShowCommand, "Показать");
                   AddWord(SystemLanguage.Russian, HideCommand, "Спрятать");

                   AddWord(SystemLanguage.Russian, Layers, "Слои");
                   AddWord(SystemLanguage.Russian, Layer, "Слой");
                   AddWord(SystemLanguage.Russian, Pallete, "Палитра");

                   AddWord(SystemLanguage.Russian, YesCommand, "Да");
                   AddWord(SystemLanguage.Russian, NoCommand, "Нет");

                   AddWord(SystemLanguage.Russian, Really, "Действительно");
                   AddWord(SystemLanguage.Russian, Error, "Ошибка");

                   AddWord(SystemLanguage.Russian, NoMaterials, "Не найдены материалы.\n" +
                                            "Вы можете добавить новые во вкладке «Наборы тайлов».");

                   AddWord(SystemLanguage.Russian, Tileset, "Набор тайлов");
                   AddWord(SystemLanguage.Russian, AutoTiles, "Авто тайлы");

                   AddWord(SystemLanguage.Russian, Draw, "Рисовать");
                   AddWord(SystemLanguage.Russian, Tilesets, "Наборы тайлов");
                   AddWord(SystemLanguage.Russian, Settings, "Настройки");

                   AddWord(SystemLanguage.Russian, PalleteScale, "Масштаб палитры");

                   AddWord(SystemLanguage.Russian, Condition, "Условие");

                   AddWord(SystemLanguage.Russian, Shaders, "Шейдеры");
                   AddWord(SystemLanguage.Russian, GridColor, "Цвет сетки");
                   AddWord(SystemLanguage.Russian, ShowGrid, "Показать сетку");

                   AddWord(SystemLanguage.Russian, SelectTileSet, "Выберите любой набор тайла, чтобы отредактировать его");

                   AddWord(SystemLanguage.Russian, NoMainTexture, "Не найдена основная текстура для набора тайлов.\n" +
                                                                  "Добавьте её и попробуйте снова.");

                   AddWord(SystemLanguage.Russian, NewTemplate, "Новый шаблон");

                   AddWord(SystemLanguage.Russian, Name, "Имя");
                   AddWord(SystemLanguage.Russian, FieldIsEmpty, "Не заполнено поле");

                   AddWord(SystemLanguage.Russian, JustTemplate, "Это просто шаблон.\n" +
                                             "Что бы использовать объект в дальнейшем, сохраните его.");

                   AddWord(SystemLanguage.Russian, ExistsError, "Точно такой же объект уже сущетсвует.\n" +
                                             "Измените что-нибудь и попробуйте снова.");

                   AddWord(SystemLanguage.English, Size, "Размер");
                }

                return _languageDictionary;
            }
        }

        public static string GetWord(string name) {
            return GetWord(name, _currentLanguage ?? Application.systemLanguage);
        }

        public static string GetWord(string name, SystemLanguage language) {
            SystemLanguage currentLanguage = language;

            if (!LanguageDictionary.ContainsKey(currentLanguage)) {
                currentLanguage = SystemLanguage.English;
            }

            Dictionary<string, string> words = LanguageDictionary[currentLanguage];

            if (!words.ContainsKey(name)) return EmptyString;

            return words[name];
        }

        public static string GetPhrase(params string[] words) {
            var stringBuilder = new StringBuilder();

            int currentWordIndex = 0;

            foreach (string word in words)
            {
                string currentWord = GetWord(word);

                if (currentWordIndex > 0)
                {
                    currentWord = currentWord.ToLower();
                }

                stringBuilder.Append(currentWord + " ");
                currentWordIndex++;
            }

            return stringBuilder.ToString();
        }

        private static void AddWord(SystemLanguage language, string name, string value) {
            if (!LanguageDictionary.ContainsKey(language)) {
                LanguageDictionary[language] = new Dictionary<string, string>();
            }
            Dictionary<string, string> words = LanguageDictionary[language];

            if (!words.ContainsKey(name)) {
                words[name] = value;
            }

        }

    }
}
