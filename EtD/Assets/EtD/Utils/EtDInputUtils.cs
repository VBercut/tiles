﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.EtD.Utils
{
    class EtDInputUtils {

        private static readonly Vector3 Zero = new Vector3();

        public static Vector3 GetMouseCoordinates(Event e) {
            return GetMouseCoordinates(e, Zero);
        }

        public static Vector3 GetMouseCoordinates(Event e, Transform relative) {
            return GetMouseCoordinates(e, relative.position);
        }

        public static Vector3 GetMouseCoordinates(Event e, Vector3 relative) {

            Vector3 pos = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x,
                          -e.mousePosition.y + Camera.current.pixelHeight)).origin;

            pos.x -= relative.x;
            pos.y -= relative.y;

            return pos;
        }

        public static Vector2 GetMouseCoordinates(Event e, Vector2 relative){

            Vector2 pos = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x,
                          -e.mousePosition.y + Camera.current.pixelHeight)).origin;

            pos.x -= relative.x;
            pos.y -= relative.y;

            return pos;
        }


        public Vector3 GetCoordinatesOnScene(Transform transform, Event e) {
            Vector3 pos = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x,
                          -e.mousePosition.y + Camera.current.pixelHeight)).origin;

            pos.x -= transform.position.x;
            pos.y -= transform.position.y;

            return pos;
        }

    }
}
