﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.EtD.Utils {

    [Serializable]
    public class EtDIntervals {

        public const string TileSetInterval = "TileSetInterval";
        public const string AutoTilePair = "AutoTilePair";
        public const string AutoTileTemplate = "AutoTileTemplate";

        [SerializeField]
        private List<string> _intervalsNames;

        private List<string> IntervalsNames {
            get { return _intervalsNames ?? (_intervalsNames = new List<string>()); }
        }

        [SerializeField]
        private List<int> _intervalsValues;

        private List<int> IntervalsValues
        {
            get { return _intervalsValues ?? (_intervalsValues = new List<int>()); }
        }

        private Dictionary<string, int> _intervalDictionary;

        private Dictionary<string, int> IntervalDictionary
        {
            get {
                if (_intervalDictionary != null) return _intervalDictionary;
                _intervalDictionary = new Dictionary<string, int>();

                for (int i = 0; i < IntervalsNames.Count; i++) {
                    _intervalDictionary[_intervalsNames[i]] = i;
                }
                return _intervalDictionary;

            }
        }


        public int UniqueNumber(string intervalName)
        {
            if (!IntervalDictionary.ContainsKey(intervalName)) {
                IntervalsNames.Add(intervalName);
                IntervalsValues.Add(0);

                IntervalDictionary[intervalName] = _intervalsNames.Count - 1;
            }

            return IntervalsValues[_intervalDictionary[intervalName]]++;
        }

    }
}
