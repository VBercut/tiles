﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.EtD.Utils
{
    public class EtDPrintUtils
    {

        public static string Print<T>(IEnumerable<T> entries)
        {
            var result = new StringBuilder("[");

            var enumerable = entries as T[] ?? entries.ToArray();
            int count = enumerable.Count();


            for (int i = 0; i < count; i++) {
                result.Append(enumerable[i]);

                if (i < count - 1)
                    result.Append(", ");
            }


            result.Append("]");

            Debug.Log(result.ToString());

            return result.ToString();
        }
    }
}
