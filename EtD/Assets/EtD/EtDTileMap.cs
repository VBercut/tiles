﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.EtD.Math;
using Assets.EtD.Serializable;
using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;

namespace Assets.EtD {
    public class EtDTileMap : MonoBehaviour, ISerializationCallbackReceiver {

        public const float MinScale = 0.5f;
        public const float MaxScale = 10f;

        public const int NoTile = -2;
        public const int JustTile = -1;

        public GUISkin Skin;

        public int Width;
        public int Height;

        public bool ShowGrid;

        public Color GridColor = new Color(0.74f, 0.69f, 0.86f, 0.7f);
        public int PixelToUnit = 100;
        public float PalleteScale = 1.0f;
        public float TileSetScale = 1.0f;
        public float TileChooserScale = 1.0f;
        public IntVector2 CellSize = new IntVector2(16, 16);

        public int CurrentTileSetIndex;
        public int CurrentTileSetChooserIndex = 0;
        public Transform CurrentLayer;

        public IntVector2 Vector;

        public bool EditMode = true;

        public const string EditorTilesContainerName = "EditorTiles";
        public const string PlayTilesContainerName = "PlayTiles";

        public const string LayersContainerName = "Layers";

        public const string LayerName = "Layer";

        private int _layerNumber = 0;

        private readonly int _tileMapHashCode = "TileMap".GetHashCode();

        public int TileMapHashCode
        {
            get { return _tileMapHashCode; }
        }


        [SerializeField]
        private Transform _layersContainer;

        public Transform LayersContainer {
            get {
                return _layersContainer = transform.FindChild(LayersContainerName) ?? EtDGameObjectUtils.AddChild(transform, LayersContainerName); 
            }
        }


        public List<EtDTileSet> TileSets
        {
            get { return _tileSets ?? (_tileSets = new List<EtDTileSet>()); }
        }


        private List<EtDTileSet> _tileSets;

        private Vector2 _tileSize;

        public Vector2 TileSize
        {
            get
            {
                _tileSize.x = (float)CellSize.x / PixelToUnit;
                _tileSize.y = (float)CellSize.y / PixelToUnit;
                return _tileSize;
            }
        }

        private Vector2 _mapSize;

        public Vector2 MapSize
        {
            get
            {
                _mapSize.x = TileSize.x * Width;
                _mapSize.y = TileSize.y * Height;
                return _mapSize;
            }
        }

        private Rect _mapBounds;

        public Rect MapBounds
        {
            get
            {
                _mapBounds.width = MapSize.x;
                _mapBounds.height = MapSize.y;
                return _mapBounds;
            }
        }

        private Vector2 _tileChooserCellSize;

        public Vector2 TileChooserCellSize
        {
            get
            {
                _tileChooserCellSize.x = CellSize.x * TileChooserScale;
                _tileChooserCellSize.y = CellSize.y * TileChooserScale;
                return _tileChooserCellSize;
            }
        }

        public Transform Layers
        {
            get { return LayersContainer.transform; }
        }


        public bool LayerExists(string layerName) {
            return LayersContainer.Find(layerName) != null;
        }

        public Transform CreateLayer() {
            string newName = LayerName + _layerNumber;

            const int maxTries = 100;
            int currentTry = 0;

            if (LayerExists(newName)) {

                while (LayerExists(newName) && currentTry < maxTries) {
                    newName = LayerName + _layerNumber;

                    _layerNumber++;
                    currentTry++;
                }

            }

            Transform newLayer = EtDGameObjectUtils.AddChild(_layersContainer, newName);
            newLayer.gameObject.AddComponent<Layer>();

            //EtDGameObjectUtils.AddChild(newLayer, EditorTilesContainerName);
            //EtDGameObjectUtils.AddChild(newLayer, PlayTilesContainerName);

            _layerNumber++;

            return newLayer;
        }

        public void CommitChanges() {
            EditorUtility.SetDirty(this);
        }


        public bool NoMaterials
        {
            get
            {
                if (TileSets.Count == 0) return true;
                return TileSets.All(tileSet => tileSet.MainTexture == null);
            }
        }

        public string[] TileSetNames {
            get {

                var tileSetsNames = new string[TileSets.Count];

                for (int i = 0; i < tileSetsNames.Length; i++) {
                    EtDTileSet tileSet = TileSets[i];
                    if (tileSet.MainTexture == null) continue;
                    tileSetsNames[i] = tileSet.Name;
                }

                return tileSetsNames;
            }
        }

        private Vector2 _tileSetCellSize;

        public Vector2 EditorTileSetCellSize {
            get {
                _tileSetCellSize.x = CellSize.x * TileSetScale;
                _tileSetCellSize.y = CellSize.y * TileSetScale;
                return _tileSetCellSize;
            }
        }

        private Vector2 _editorCellSize;

        public Vector2 EditorDrawCellSize
        {
            get
            {
                _editorCellSize.x = CellSize.x * PalleteScale;
                _editorCellSize.y = CellSize.y * PalleteScale;
                return _editorCellSize;
            }
        }

        private Vector2 _size;

        public Vector2 Size {
            get {
                _size.Set(Width, Height);
                return _size;
            }
            set {
                _size = value;
                Width = (int) _size.x;
                Height = (int) _size.y;
            }
        }


        [SerializeField]
        private EtDIntervals _intervals;

        public EtDIntervals Intervals {
            get { return _intervals ?? (_intervals = new EtDIntervals()); }
        }


        private List<EtDAutoTilePair> _autoTilePairs;

        public List<EtDAutoTilePair> AutoTilePairs
        {
            get { return _autoTilePairs ?? (_autoTilePairs = new List<EtDAutoTilePair>()); }
        }

        public void AddTileSet(Material material) {
            TileSets.Add(new EtDTileSet(material, this));

            EtDTileSet tileSet = TileSets.Last();
            MaterialDictionary[tileSet.Material.GetInstanceID()] = tileSet.Material;
        }



        public void Start() {
            
        }

        public void OnDrawGizmos() {
            Gizmos.color = GridColor;

            Gizmos.DrawWireCube(
                new Vector3(transform.position.x + MapSize.x / 2, transform.position.y - MapSize.y / 2,
                        transform.position.z),
                new Vector3(MapSize.x, MapSize.y, 0));

            if (ShowGrid) {
                for (int sx = 0; sx < Width; sx++) {
                    Gizmos.DrawLine(new Vector3(transform.position.x + sx * TileSize.x, transform.position.y),
                                    new Vector3(transform.position.x + sx * TileSize.x, transform.position.y - MapSize.y));
                }

                for (int sy = 0; sy < Height; sy++) {
                    Gizmos.DrawLine(new Vector3(transform.position.x, transform.position.y - sy * TileSize.y),
                                    new Vector3(transform.position.x + MapSize.x, transform.position.y - sy * TileSize.y));
                }
            }

            Gizmos.color = Color.clear;

            Gizmos.DrawCube(
                new Vector3(transform.position.x + MapSize.x / 2, transform.position.y - MapSize.y / 2,
                        transform.position.z),
                new Vector3(MapSize.x, MapSize.y, 0));

        }


        public void RemoveTileSet(EtDTileSet tileSet) {
            if (tileSet == null) return;

            TileSets.Remove(tileSet);
            MaterialDictionary.Remove(tileSet.Material.GetInstanceID());
            
            // Don't forget remove all dependencies
            // It's really complex method
        }

        public List<EtDAutoTileTemplate> FindAllTemplates() {
            return TileSets.SelectMany(tileSet => tileSet.Templates).ToList();
        }

        // Update serialization for tileSets
        public List<TileSetSerializable> TileSetSerializables
        {
            get { return _tileSetSerializables ?? (_tileSetSerializables = new List<TileSetSerializable>()); }
        }

        [SerializeField]
        private List<TileSetSerializable> _tileSetSerializables;


        public List<AutoTilePairSerializable> AutoTilePairsSerializables
        {
            get { return _autoTilePairsSerializables ?? (_autoTilePairsSerializables = new List<AutoTilePairSerializable>()); }
        }

        [SerializeField]
        private List<AutoTilePairSerializable> _autoTilePairsSerializables;


        public void OnBeforeSerialize() {
            TileSetSerializables.Clear();
            foreach (var tileSet in TileSets) {
                TileSetSerializables.Add(tileSet.Serializable());
            }

            AutoTilePairsSerializables.Clear();
            foreach (var pair in AutoTilePairs) {
                AutoTilePairsSerializables.Add(pair.Serializable());  
            }
        }

        public void OnAfterDeserialize() {
            TileSets.Clear();
            foreach (var tileSet in TileSetSerializables) {
                TileSets.Add(tileSet.Deserializable(this));
            }

            AutoTilePairs.Clear();
            foreach (var pair in AutoTilePairsSerializables) {
                AutoTilePairs.Add(pair.Deserializable(this));
            }
        }

        private Dictionary<int, EtDAutoTilePair> _autoTilePairDictionary;

        private Dictionary<int, EtDAutoTilePair> AutoTilePairDictionary
        {
            get {

                if (_autoTilePairDictionary == null) {
                    _autoTilePairDictionary = new Dictionary<int, EtDAutoTilePair>();

                    foreach (var pair in AutoTilePairs) {
                        _autoTilePairDictionary[pair.ID] = pair;
                    }

                }

                return _autoTilePairDictionary;
            }
        }

        private Dictionary<int, Material> _materialDictionary;

        public Dictionary<int, Material> MaterialDictionary {
            get {
                if (_materialDictionary == null) {
                    _materialDictionary = new Dictionary<int, Material>();

                    foreach (var tileSet in TileSets) {
                        _materialDictionary[tileSet.Material.GetInstanceID()] = tileSet.Material;
                    }
                }
                return _materialDictionary;
            }
        }


        public Material FindMaterial(int id) {
            if(!MaterialDictionary.ContainsKey(id)) return null;
            return MaterialDictionary[id];
        }


        private Dictionary<int, EtDAutoTilePair> _autoTilePairsDictionary;

        private Dictionary<int, EtDAutoTilePair> AutoTilePairsDictionary {
            get
            {

                if (_autoTilePairsDictionary == null) {
                    _autoTilePairsDictionary = new Dictionary<int, EtDAutoTilePair>();

                    foreach (var pair in AutoTilePairs) {
                        _autoTilePairsDictionary[pair.FirstId ^ pair.SecondId] = pair;
                    }

                }

                return _autoTilePairsDictionary;
            }
        }

        public EtDAutoTilePair FindPair(int pairId) {
            if(!AutoTilePairDictionary.ContainsKey(pairId)) return null;

            return AutoTilePairDictionary[pairId];
        }

        public EtDAutoTilePair FindPair(int firstId, int secondId) {
            if (!AutoTilePairsDictionary.ContainsKey(firstId ^ secondId)) return null;

            return AutoTilePairsDictionary[firstId ^ secondId];
        }

        public void AddAutoTilePair(EtDAutoTilePair pair) {
            if (AutoTilePairDictionary.ContainsKey(pair.ID)) return;
            AutoTilePairDictionary[pair.ID] = pair;
            AutoTilePairsDictionary[pair.FirstId ^ pair.SecondId] = pair;
            AutoTilePairs.Add(pair);
        }


        private Dictionary<int, EtDAutoTileTemplate> _templatesDictionary;

        private Dictionary<int, EtDAutoTileTemplate> TemplatesDictionary
        {
            get { return _templatesDictionary ?? (_templatesDictionary = new Dictionary<int, EtDAutoTileTemplate>()); }
        }

        private Dictionary<IntVector2, EtDAutoTileTemplate> _templatesPositionDictionary;

        private Dictionary<IntVector2, EtDAutoTileTemplate> TemplatesPositionDictionary {
            get { return _templatesPositionDictionary ?? (_templatesPositionDictionary = new Dictionary<IntVector2, EtDAutoTileTemplate>()); }
        }



        public int DoubleWidth {
            get { return 2 * Width; }
        }

        public int DoubleHeight {
            get { return 2 * Height; }
        }

        public EtDAutoTileTemplate FindTemplate(int id) {
            if(!TemplatesDictionary.ContainsKey(id)) return null;
            return TemplatesDictionary[id];
        }

        public EtDAutoTileTemplate FindTemplate(IntVector2 position) {
            if (!TemplatesPositionDictionary.ContainsKey(position)) return null;
            return TemplatesPositionDictionary[position];
        }

        public void AddAutoTileTemplate(EtDAutoTileTemplate template) {
            if (TemplatesDictionary.ContainsKey(template.ID)) return;
            TemplatesDictionary[template.ID] = template;
            TemplatesPositionDictionary[template.Position] = template;
        }

        public void RemoveAutoTileTemplate(EtDAutoTileTemplate template) {
            if (!TemplatesDictionary.ContainsKey(template.ID)) return;
            TemplatesDictionary.Remove(template.ID);
            TemplatesPositionDictionary.Remove(template.Position);
        }

        public void AddTile(Transform layer, Tile tile) {
            if(layer == null) return;

            var customLayer = layer.gameObject.GetComponent<Layer>();

            var localCoords = tile.transform.position - layer.position;
            var positionOnMap = new IntVector2(Mathf.RoundToInt(localCoords.x / TileSize.x), Mathf.RoundToInt(localCoords.y / TileSize.y) * -1);
            if (!CanPlaceTile(positionOnMap)) return;

            if (customLayer != null) {
                customLayer.AddTile(tile, positionOnMap);
                return;
            }

            Transform editorContainer = layer.FindChild(EditorTilesContainerName);
            if(editorContainer == null) return;

            Transform newTile = EtDGameObjectUtils.AddChild(editorContainer, TileName(positionOnMap), true);
            localCoords.z = layer.localPosition.z;
            newTile.localPosition = localCoords;

            //Debug.Log(localCoords);
            //Debug.Log(positionOnMap);

            //Debug.Log("==================== The Lovely manager ==================");

            var tileContainer = newTile.gameObject.AddComponent<TileContainer>();
            tileContainer.CreateTile(this, tile.MeshRenderer.sharedMaterial, tile.TexturePosition, CellSize, TileSize);

            //if(tileContainer.AutoTileTemplate == null) return;

            // Auto-tiling rules
 
            // -- 0 -- -- 1 -- -- 2 --
            // -- 3 -- -- 4 -- -- 5 --
            // -- 6 -- -- 7 -- -- 8 --

            var otherTiles = new List<Transform> {
                FindTile(new IntVector2((int) positionOnMap.x - 1, (int) positionOnMap.y - 1), layer),  // 0
                FindTile(new IntVector2((int) positionOnMap.x, (int) positionOnMap.y - 1), layer),      // 1
                FindTile(new IntVector2((int) positionOnMap.x + 1, (int) positionOnMap.y - 1), layer),  // 2

                FindTile(new IntVector2((int) positionOnMap.x - 1, (int) positionOnMap.y), layer),      // 3
                FindTile(new IntVector2((int) positionOnMap.x, (int) positionOnMap.y), layer),          // 4
                FindTile(new IntVector2((int) positionOnMap.x + 1, (int) positionOnMap.y), layer),      // 5

                FindTile(new IntVector2((int) positionOnMap.x - 1, (int) positionOnMap.y + 1), layer),  // 6
                FindTile(new IntVector2((int) positionOnMap.x, (int) positionOnMap.y + 1), layer),      // 7
                FindTile(new IntVector2((int) positionOnMap.x + 1, (int) positionOnMap.y + 1), layer)   // 8
            };

            Template template;

            try {
                // Left Top tileCorner 0, 1, 3, 4
                template = Template.CreateTemplate(new[] { 0, 1, 3, 4 }, otherTiles);
                var correctPair = FindPair(template.First, template.Second);

                if (template.Exists && correctPair != null) { 
                    var container = correctPair.FindTile(template.Result, tileContainer.AutoTileTemplate.ID);

                    SetTemplateToConer(template: 0, cornerIndex: 3, otherTiles: otherTiles, tileCorner: container.LeftTopCorner);
                    SetTemplateToConer(template: 1, cornerIndex: 2, otherTiles: otherTiles, tileCorner: container.RightTopCorner);
                    SetTemplateToConer(template: 3, cornerIndex: 1, otherTiles: otherTiles, tileCorner: container.LeftBottomCorner);
                    SetTemplateToConer(template: 4, cornerIndex: 0, otherTiles: otherTiles, tileCorner: container.RightBottomCorner);
                }
            } catch (Exception exception) {
                //Debug.Log(exception);
            }

            
            try {

                // Right Top tileCorner 1, 2, 4, 5
                template = Template.CreateTemplate(new[] { 1, 2, 4, 5 }, otherTiles);
                if (template.Exists) {
                    var correctPair = FindPair(template.First, template.Second);
                    var container = correctPair.FindTile(template.Result, tileContainer.AutoTileTemplate.ID);

                    SetTemplateToConer(template: 1, cornerIndex: 3, otherTiles: otherTiles, tileCorner: container.LeftTopCorner);
                    SetTemplateToConer(template: 2, cornerIndex: 2, otherTiles: otherTiles, tileCorner: container.RightTopCorner);
                    SetTemplateToConer(template: 4, cornerIndex: 1, otherTiles: otherTiles, tileCorner: container.LeftBottomCorner);
                    SetTemplateToConer(template: 5, cornerIndex: 0, otherTiles: otherTiles, tileCorner: container.RightBottomCorner);
                }
            } catch (Exception exception) {
                //Debug.Log(exception);
            }

            try {

                // Left Bottom tileCorner 3, 4, 6, 7
                template = Template.CreateTemplate(new[] { 3, 4, 6, 7 }, otherTiles);
                if (template.Exists) {
                    var correctPair = FindPair(template.First, template.Second);
                    var container = correctPair.FindTile(template.Result, tileContainer.AutoTileTemplate.ID);

                    SetTemplateToConer(template: 3, cornerIndex: 3, otherTiles: otherTiles, tileCorner: container.LeftTopCorner);
                    SetTemplateToConer(template: 4, cornerIndex: 2, otherTiles: otherTiles, tileCorner: container.RightTopCorner);
                    SetTemplateToConer(template: 6, cornerIndex: 1, otherTiles: otherTiles, tileCorner: container.LeftBottomCorner);
                    SetTemplateToConer(template: 7, cornerIndex: 0, otherTiles: otherTiles, tileCorner: container.RightBottomCorner);
                }
            } catch (Exception exception) {
                //Debug.Log(exception);
            }

            try {

                // Left Bottom tileCorner 4, 5, 7, 8
                template = Template.CreateTemplate(new[] { 4, 5, 7, 8 }, otherTiles);
                if (template.Exists) {
                    var correctPair = FindPair(template.First, template.Second);
                    var container = correctPair.FindTile(template.Result, tileContainer.AutoTileTemplate.ID);

                    SetTemplateToConer(template: 4, cornerIndex: 3, otherTiles: otherTiles, tileCorner: container.LeftTopCorner);
                    SetTemplateToConer(template: 5, cornerIndex: 2, otherTiles: otherTiles, tileCorner: container.RightTopCorner);
                    SetTemplateToConer(template: 7, cornerIndex: 1, otherTiles: otherTiles, tileCorner: container.LeftBottomCorner);
                    SetTemplateToConer(template: 8, cornerIndex: 0, otherTiles: otherTiles, tileCorner: container.RightBottomCorner);
                }

// ReSharper disable once EmptyGeneralCatchClause
            } catch (Exception exception) {
            }
            
        }

        private void SetTemplateToConer(int template, int cornerIndex, List<Transform> otherTiles, EtDTile tileCorner) {
            Transform currentTransform = otherTiles[template];
            if(currentTransform == null) return;

            try {
                var container = currentTransform.GetComponent<TileContainer>();
                var corner = container.Corners[cornerIndex];


                corner.MeshRenderer.sharedMaterial = tileCorner.Material;
                corner.MeshFilter.sharedMesh.uv = tileCorner.Mesh.uv;
                corner.MeshFilter.sharedMesh.RecalculateNormals();
            }
            catch (Exception exception) {
                //Debug.LogException(exception);
            }
        }

        private class Template {
            private int _first = NoTile;
            private int _second = NoTile;

            private List<int> _result;

            public bool Exists {
                get { return _result.Count > 0; }
            }

            public int First {
                get { return _first; }
            }

            public int Second {
                get { return _second; }
            }

            public IEnumerable<int> Result {
                get { return _result; }
            } 

            public static Template CreateTemplate(IEnumerable<int> entires, List<Transform> tiles) {
                var obj = new Template {_result = new List<int>()};

                var resultCount = new Dictionary<int, int>();

                foreach (var index in entires) {
                    var currentTile = tiles[index] == null ? null : tiles[index].gameObject.GetComponent<TileContainer>();

                    int currentId = currentTile == null ? NoTile :
                    currentTile.AutoTileTemplate == null ? JustTile : currentTile.AutoTileTemplate.ID;

                    obj._result.Add(currentId);

                    if (!resultCount.ContainsKey(currentId))
                    {
                        resultCount[currentId] = 1;
                    }
                    else
                    {
                        resultCount[currentId]++;
                    }

                    if (resultCount.Count > 2)
                    {
                        obj._result.Clear();
                        break;
                    }
                }

                int currentKey = 0;
                foreach (var key in resultCount.Keys) {
                    if (currentKey == 0) {
                        obj._first = obj._second = key;
                    }

                    if (currentKey == 1) {
                        obj._second = key;
                    }

                    currentKey++;
                }

                if (obj._first < 0 && obj._second < 0) {
                    obj._result.Clear();
                }

                if (obj._first >= 0 && obj._second < 0) {
                    obj._second = obj._first;
                }

                if (obj._first < 0 && obj._second >= 0) {
                    obj._first = obj._second;
                }


                return obj;
            }
        }


        public static string TileName(int x, int y) {
            return (new StringBuilder("Tile_")).Append(x).Append("_").Append(y).ToString();
        }

       public static string TileName(IntVector2 position) {
            return TileName(position.x, position.y);
       }

       public static string TileName(Vector2 position) {
           return TileName((int) position.x, (int) position.y);
       }

        public Transform FindTile(IntVector2 position, Transform layer) {
            if (!CanPlaceTile(position)) return null;
            if (layer == null) return null;

            Transform editorContainer = layer.FindChild(EditorTilesContainerName);
            if (editorContainer == null) return null;

            return editorContainer.FindChild(TileName(position.x, position.y));
        }

        private bool CanPlaceTile(IntVector2 position) {
            return !(position.x < 0 || position.y < 0 || position.x >= Width || position.y >= Height);
        }

        private bool CanPlaceTile(Vector2 position) {
            return CanPlaceTile(new IntVector2((int) position.x, (int) position.y));
        }

        public void RemoveTile(IntVector2 position, Transform layer) {
            if (layer == null) return;
            Transform editorContainer = layer.FindChild(EditorTilesContainerName);
            if (editorContainer == null) return;

            Transform tile = FindTile(position, layer);
            if(tile != null) DestroyImmediate(tile.gameObject);
        }

        public void RemoveTile(int x, int y, Transform layer) {
            RemoveTile(new IntVector2(x, y), layer);
        }
    }
}
