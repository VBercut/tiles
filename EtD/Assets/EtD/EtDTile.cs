﻿using System;
using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.EtD {

    [Serializable]
    public class EtDTile {

        [SerializeField]
        private Material _material;

        [SerializeField]
        private IntVector2 _position;

        [SerializeField] 
        private Mesh _mesh;

        public Mesh Mesh {
            get { return _mesh; }
        }

        public Material Material {
            get { return _material; }
        }

        public IntVector2 Position {
            get { return _position; }
        }

        public void BuildTile(Material material, IntVector2 position, IntVector2 tileSize, Vector2 meshSize) {
            _material = material;
            _position = position;

            if (_mesh) Object.DestroyImmediate(_mesh);

            _mesh = EtDMeshUtils.BuildRectMesh(meshSize, material, position, tileSize);
            //_mesh.hideFlags = HideFlags.DontSave;
        }
    }
}
