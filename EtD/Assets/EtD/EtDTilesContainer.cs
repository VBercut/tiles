﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEngine;

namespace Assets.EtD {
    [ExecuteInEditMode]
    class EtDTilesContainer : MonoBehaviour {

        private List<Tile> _tiles;

        private static readonly IntVector2 PositiveDirection = new IntVector2(1, 1);

        public List<Tile> Tiles {
            get { return _tiles ?? (_tiles = new List<Tile>()); }
        }

        public IntVector2 Sizes;

        public void AddTile(Material material, IntVector2 startPosition, IntVector2 tileSize, Vector2 meshSize, IntVector2 direction = null) {
            Tiles.Add(CreateTile(material, startPosition, tileSize, meshSize, direction));
        }

        public void InsertTile(int position, Material material, IntVector2 startPosition, IntVector2 tileSize, Vector2 meshSize, IntVector2 direction = null) {
            Tiles.Insert(position, CreateTile(material, startPosition, tileSize, meshSize, direction));

            var newTile = Tiles[position];
            var previousTile = Tiles[position - 1];
            var newTileDirection = direction ?? PositiveDirection;
            var tilePosition = new IntVector2(previousTile.Position.x + 1, previousTile.Position.y);

            newTile.Position = tilePosition;
            newTile.transform.localPosition = new Vector3(meshSize.x * tilePosition.x * newTileDirection.x,
                                                         -meshSize.y * tilePosition.y * newTileDirection.y);
        }

        public Tile CreateTile(Material material, IntVector2 startPosition, IntVector2 tileSize, Vector2 meshSize, IntVector2 direction = null) {

                var newTileDirection = direction ?? PositiveDirection;
                var tilePosition = new IntVector2();

                if (Tiles.Count > 0) {
                    Tile lastTile = Tiles.Last();

                    tilePosition.x = lastTile.Position.x + 1;
                    tilePosition.y = lastTile.Position.y;

                    if (tilePosition.x >= Sizes.x) {
                        tilePosition.x = 0;
                        tilePosition.y++;
                    }
                }

                Transform tileTransform = EtDGameObjectUtils.AddChild(transform, "Tile_" + tilePosition);
                tileTransform.localPosition = new Vector3(meshSize.x * tilePosition.x * newTileDirection.x,
                                                          -meshSize.y * tilePosition.y * newTileDirection.y);

                var tile = tileTransform.gameObject.AddComponent<Tile>();

                tile.CreateTile(material, startPosition, tileSize, meshSize);
                tile.Position = tilePosition;
                tile.TexturePosition = startPosition;
            
            return tile;
        }

        public void Clear() {
            for (int i = Tiles.Count - 1; i > 0; i--) {
                var tile = Tiles[i];
                Tiles.Remove(tile);

                DestroyImmediate(tile.gameObject);
            }
        }

        // Add line
        public void AddRow(Material material, Rect selectedTiles, IntVector2 tileSize, Vector2 meshSize, IntVector2 direction = null) {
            Sizes.y++;

            for (int x = 0; x < Sizes.x; x++) {
                AddTile(material, CalculatePosition(x, Sizes.y, selectedTiles), tileSize, meshSize, direction);
            }
        }

        public void AddColumn(Material material, Rect selectedTiles, IntVector2 tileSize, Vector2 meshSize, IntVector2 direction = null) {

            for (int y = Sizes.y - 1; y > 0; y--) {
                InsertTile(y * Sizes.x, material, CalculatePosition(Sizes.x, y, selectedTiles), tileSize, meshSize, direction);
            }

            Sizes.x++;

            AddTile(material, CalculatePosition(Sizes.x, Sizes.y, selectedTiles), tileSize, meshSize, direction);
        }


        // Remove line
        public void RemoveRow() {
            Sizes.y--;

            for (int i = Tiles.Count - 1; i >= Sizes.x * Sizes.y; i--) {
                var tile = Tiles[i];
                Tiles.Remove(tile);

                DestroyImmediate(tile.gameObject);
            }
        }

        // Remove column
        public void RemoveColumn() {

            for (int y = Sizes.y - 1; y >= 0; y--) {
                int index = y * Sizes.x + Sizes.x - 1;
                var tile = Tiles[index];
                Tiles.Remove(tile);

                DestroyImmediate(tile.gameObject);
            }

            Sizes.x--;
        }

        public IntVector2 CalculatePosition(int x, int y, Rect selectedTiles) {
            return new IntVector2((int)(selectedTiles.x + (x % selectedTiles.width)),
                                  (int) (selectedTiles.y + (y % selectedTiles.height)));
        }

    }
}
