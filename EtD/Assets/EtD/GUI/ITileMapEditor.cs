﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.EtD.GUI
{
    public interface ITileMapEditor {
        void ShowAutoTileEditorWindow(EtDTileMap tileMap, EtDAutoTileTemplate autoTileTemplate);
        EtDTileMaker TileMaker { get; }
    }
}
