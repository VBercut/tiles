﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.EtD.GUI
{
    public interface IGuiComponent : ISubEditor
    {
        Vector2 Position { get; set; }

        Vector2 Size { get; set; }

        Rect Bounds { get; }
        Rect Area { get; }

        Transform Transform { get; }

        IEnumerable<IGuiComponent> Children { get; }

        bool Alive { get; set; }
        bool Active { get; set; }

        void HandleInputOnInspectorGUI();
        void HandleInputOnSceneGUI();

        void AddChild(IGuiComponent child);
        void RemoveChild(IGuiComponent child);

        bool Hit(Vector2 point);

        void SetSize(float x, float y);
    }
}
