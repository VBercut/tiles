﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.EtD.Utils;
using UnityEngine;

namespace Assets.EtD.GUI
{
    public abstract class EtDContainer : IGuiComponent {

        public Vector2 Position { get;
            set;
        }

        public Vector2 Size { get; set; }

#pragma warning disable 649
        private Rect _bounds;
#pragma warning restore 649

        public Rect Bounds {
            get {
               _bounds.Set(Position.x, Position.y, Size.x, Size.y);
               return _bounds;
            }
        }

        public abstract Rect Area { get; }

        public abstract Transform Transform { get; }

        private List<IGuiComponent> _children; 

        public IEnumerable<IGuiComponent> Children
        {
            get { return _children ?? (_children = new List<IGuiComponent>()); }
        }

        private bool _alive = true;

        public bool Alive
        {
            get { return _alive; }
            set { _alive = value; }
        }

        public virtual bool Active { get; set; }

        protected Vector2 MousePosition {
            get { return EtDInputUtils.GetMouseCoordinates(Event.current, Position); }
        }

        private float _horizontalPadding = 5;
        private float _verticalPadding = 5;


        public float Padding {
            get { return System.Math.Max(_horizontalPadding, _verticalPadding); }
            set { _horizontalPadding = _verticalPadding = value; }
        }


        public virtual void OnInspectorGUI() {

            foreach (var child in Children) {
                child.OnInspectorGUI();

                if (child.Hit(MousePosition))
                {
                    child.HandleInputOnInspectorGUI();
                }

            }
        }
      
        public void OnSceneGUI() {
            foreach (var child in Children)
            {
                child.OnInspectorGUI();

                if (child.Hit(MousePosition))
                {
                    child.HandleInputOnInspectorGUI();
                }

            }
        }

        public abstract void HandleInputOnInspectorGUI();
        public abstract void HandleInputOnSceneGUI();

        public virtual void AddChild(IGuiComponent child) {
            Children.ToList().Add(child);
        }

        public void RemoveChild(IGuiComponent child) {
            Children.ToList().Remove(child);
        }

        public bool Hit(Vector2 point) {
            return Bounds.Contains(point) && Alive;
        }

        public void SetSize(float x, float y) {
            Size = new Vector2(x, y);
        }
    }
}
