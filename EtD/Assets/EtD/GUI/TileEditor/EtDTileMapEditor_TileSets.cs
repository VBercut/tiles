using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;

namespace Assets.EtD.GUI.TileEditor
{
    public class EtDTileMapEditorTileSets : ISubEditor {

        private int _currentShader;
        private Vector2 _tileSetListPos;

        private readonly EtDTileMap _parent;
        private readonly ITileMapEditor _editor;
        private EtDTileSet _currentTileSet;
        private Vector2 _tileSetPos;

        private readonly IntVector2 _currentTilePosition;
        private bool _buttonsLoaded;

        public EtDTileMapEditorTileSets(EtDTileMap parent, ITileMapEditor editor) {
            _parent = parent;
            _editor = editor;
            _currentTilePosition = new IntVector2();
        }

        public void OnInspectorGUI() {

            //Tile sets list
            DrawTileSets();

            // Current tile set
            DrawCurrentTileSet();
        }

        private void DrawTileSets() {
            EtDTileSet deletedTileSet = null;

            // Tile set list
            _tileSetListPos = GUILayout.BeginScrollView(_tileSetListPos);

            foreach (var tileSet in _parent.TileSets)
            {
                UnityEngine.GUI.color = Equals(_currentTileSet, tileSet) ? Color.green : UnityEngine.GUI.color;

                EditorGUILayout.BeginVertical(UnityEngine.GUI.skin.GetStyle("Box"));

                tileSet.Name = GUILayout.TextField(tileSet.Name);

                GUILayout.Label(tileSet.TextureName);

#pragma warning disable 618
                tileSet.MainTexture = (Texture)EditorGUILayout.ObjectField(tileSet.MainTexture, typeof(Texture));
#pragma warning restore 618


                GUILayout.Label(tileSet.ShaderName);

                Editor.CreateEditor(tileSet.Material).OnInspectorGUI();

                if (EtDGUIUtils.Button(EtDEditorDictionary.GetWord(EtDEditorDictionary.DeleteCommand), Color.red))
                {

                    if (!EtDGUIUtils.ShowDialog(EtDEditorDictionary.GetPhrase(EtDEditorDictionary.DeleteCommand, EtDEditorDictionary.Tileset)))
                    {
                        deletedTileSet = tileSet;
                    }
                }

                EditorGUILayout.EndVertical();

                // Handle input
                HandleTileSetsInput(GUILayoutUtility.GetLastRect(), tileSet);

                UnityEngine.GUI.color = Color.white;
            }

            EditorGUILayout.EndScrollView();


            // Add new tile set
            EditorGUILayout.BeginVertical();

            GUILayout.Label(EtDEditorDictionary.GetWord(EtDEditorDictionary.Shaders));
            _currentShader = EditorGUILayout.Popup(_currentShader, EtDShaderPool.ShadersList, GUILayout.ExpandWidth(true));

            if (EtDGUIUtils.Button(EtDEditorDictionary.GetWord(EtDEditorDictionary.AddCommand), Color.yellow))
            {
                Material material = EtDShaderPool.CreateMaterial(EtDShaderPool.ShadersList[_currentShader], null);
                //material.hideFlags = HideFlags.DontSave;

                _parent.AddTileSet(material);
            }

            EditorGUILayout.EndVertical();

            PostProcessing(deletedTileSet);
        }

        private void HandleTileSetsInput(Rect area, EtDTileSet tileSet) {
            if (Event.current.type == EventType.MouseDown && area.Contains(Event.current.mousePosition)) {
                _currentTileSet = tileSet;
                Event.current.Use();
            }

        }

        private void PostProcessing(EtDTileSet deletedTileSet) {
            _parent.RemoveTileSet(deletedTileSet);
        }




        private void DrawCurrentTileSet() {
            if (_currentTileSet == null) {
                EtDGUIUtils.Label(EtDEditorDictionary.GetWord(EtDEditorDictionary.SelectTileSet), Color.gray);
                return;
            }

            if (_currentTileSet.MainTexture == null) {
                EtDGUIUtils.Label(EtDEditorDictionary.GetWord(EtDEditorDictionary.NoMainTexture), Color.red);
                return;
            }

            _parent.TileSetScale = EditorGUILayout.Slider(EtDEditorDictionary.GetWord(EtDEditorDictionary.PalleteScale), _parent.TileSetScale, EtDTileMap.MinScale,
                               EtDTileMap.MaxScale);

            Texture tileSetTexture = _currentTileSet.MainTexture;
            float palleteScale = _parent.TileSetScale;

            var viewRect = new Rect(0, 0, tileSetTexture.width, tileSetTexture.height);
            viewRect.width *= palleteScale;
            viewRect.height *= palleteScale;

            _tileSetPos = EditorGUILayout.BeginScrollView(_tileSetPos);

            UnityEngine.GUI.DrawTexture(viewRect, tileSetTexture, ScaleMode.StretchToFill, true, palleteScale - 1);
            GUILayoutUtility.GetRect(viewRect.width, viewRect.height);

            DrawSelectedTiles(GUILayoutUtility.GetLastRect());

            EditorGUILayout.EndScrollView();

            HandleInputTileSet(GUILayoutUtility.GetLastRect());

            DrawAddAutoTilePanel();
        }

        private void DrawSelectedTiles(Rect area) {
            Vector2 cellSize = _parent.EditorTileSetCellSize;

            var highlightRect = new Rect(area.x + _currentTilePosition.x * cellSize.x,
                                         area.y + _currentTilePosition.y * cellSize.y,
                                         cellSize.x, cellSize.y);

            EtDDrawUtils.DrawSolidRectangleWithOutline(highlightRect);
        }

        private void HandleInputTileSet(Rect area) {
            int controlId = GUIUtility.GetControlID(FocusType.Passive, area);

            if (!area.Contains(Event.current.mousePosition)) return;

            switch (Event.current.GetTypeForControl(controlId))
            {
                case EventType.MouseDown:
                    Vector2 localClickPosition = Event.current.mousePosition - new Vector2(area.x, area.y)
                                                 + new Vector2(_tileSetPos.x, _tileSetPos.y);

                    _currentTilePosition.x = (int)(localClickPosition.x / _parent.EditorTileSetCellSize.x);
                    _currentTilePosition.y = (int)(localClickPosition.y / _parent.EditorTileSetCellSize.y);

                    GUIUtility.hotControl = controlId;
                    HandleUtility.Repaint();
                    break;

                case EventType.MouseUp:
                    GUIUtility.hotControl = 0;
                    HandleUtility.Repaint();
                    break;
            }
        }


        private void DrawAddAutoTilePanel() {
           EtDAutoTileTemplate template =  _currentTileSet.FindAutoTileTemplate(_currentTilePosition);

            if (template != null) {

                if (!_buttonsLoaded && Event.current.type == EventType.Layout) {
                    _buttonsLoaded = true;
                }

                if (_buttonsLoaded) {

                    template.Name = EditorGUILayout.TextField(EtDEditorDictionary.GetWord(EtDEditorDictionary.Name), template.Name);

                    EditorGUILayout.BeginHorizontal();

                    if (EtDGUIUtils.Button(EtDEditorDictionary.GetWord(EtDEditorDictionary.EditCommand), Color.magenta,
                        Screen.width * 0.3f)) {
                        // Show window
                        _editor.ShowAutoTileEditorWindow(_parent, template);
                    }

                    if (EtDGUIUtils.Button(EtDEditorDictionary.GetWord(EtDEditorDictionary.DeleteCommand), Color.red,
                        Screen.width * 0.3f)) {
                        _currentTileSet.RemoveAutoTileTemplate(_currentTilePosition);
                    }


                    EditorGUILayout.EndHorizontal();
                }

            } else {
                if (EtDGUIUtils.Button(EtDEditorDictionary.GetWord(EtDEditorDictionary.CreateCommand), Color.yellow,
                    Screen.width * 0.3f)) {
                    _currentTileSet.AddAutoTileTemplate(_currentTilePosition);
                }

                _buttonsLoaded = false;
            }
        }



        public void OnSceneGUI()
        {
            
        }
    }
}
