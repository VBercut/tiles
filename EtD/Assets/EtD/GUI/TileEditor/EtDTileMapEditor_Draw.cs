﻿using System.Collections.Generic;
using System.Linq;
using Assets.EtD.GUI.SubItems;
using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.EtD.GUI.TileEditor
{
    public class EtDTileMapEditorDraw : ISubEditor, ILayerChanger {

        private const float LayerHeight = 45;
        private const int MaxVisibleLayersInList = 5;

        private readonly List<IGuiComponent> _layers;
        private readonly EtDTileMap _parent;

        private bool _showLayers = true;
        private bool _showPalette = true;
        private Vector2 _layerListPos;
        private IGuiComponent _newActiveLayer;

        private static readonly IComparer<IGuiComponent> Comparer = new DraggbleComparer();
        private static readonly IComparer<IGuiComponent> ZComparer = new LayerZComparer();
        private Vector2 _palettePos;

        private IntVector2 _tileFirst;
        private IntVector2 _tileLast;

        private ITileMapEditor _editor;

        private const float DeltaZ = 0.1f;

        private EtDTileMaker TileMaker {
            get { return _editor.TileMaker; }
        }

        public EtDTileMapEditorDraw(EtDTileMap parent, ITileMapEditor editor) {
            _layers = new List<IGuiComponent>();
            _parent = parent;
            _editor = editor;

            _tileFirst = new IntVector2(-1, -1);
            _tileLast = new IntVector2(-1, -1);

            BuildLayersView();
        }

        private void BuildLayersView() {

            foreach (Transform layer in _parent.Layers) {
               AddLayer(layer); 
            }

            ResortLayers();
        }

        public void ResortLayers() {
            _layers.Sort(ZComparer);

            for (int currentLayer = 0; currentLayer < _layers.Count; currentLayer++) {
                Vector2 layerPosition = _layers[currentLayer].Position;
                layerPosition.y = currentLayer * LayerHeight;

                _layers[currentLayer].Position = layerPosition;
            }
        }


        public void OnInspectorGUI() {

            _showLayers = EditorGUILayout.Foldout(_showLayers, EtDEditorDictionary.GetWord(EtDEditorDictionary.Layers));
            if (_showLayers) DrawLayers();

            _showPalette = EditorGUILayout.Foldout(_showPalette, EtDEditorDictionary.GetWord(EtDEditorDictionary.Pallete));
            if (_showPalette) DrawPallete();

            HandleUtility.Repaint();     
        }

        private void DrawLayers() {

            // Begin List of Layers

            DrawLayersStartUp();

            float listHeight = System.Math.Min(MaxVisibleLayersInList * LayerHeight, _layers.Count * LayerHeight);  

            _layerListPos = EditorGUILayout.BeginScrollView(_layerListPos, GUILayout.ExpandWidth(true),
                                                                           GUILayout.Height(listHeight));

            foreach (var layer in _layers) {
                layer.OnInspectorGUI();
                HandlerInput(layer);
            }

            EditorGUILayout.EndScrollView();

            CheckActiveLayer();
            SortLayers();
            CheckDeleteLayer();

            // End List of Layers


            // Create layer panel
            CreateLayerPanel();
        }

        private void SortLayers() {
            if (Event.current.type == EventType.MouseUp) {

                float currentY = 0;
                float currentZ = 0;

                _layers.Sort(Comparer);

                foreach (IGuiComponent layer in _layers) {

                    Vector2 position = layer.Position;
                    position.y = currentY;
                    layer.Position = position;

                    Vector3 layerPosition = layer.Transform.localPosition;
                    layerPosition.z = currentZ;
                    layer.Transform.localPosition = layerPosition;

                    currentY += LayerHeight;
                    currentZ += DeltaZ;
                }
            }
        }

        private void DrawLayersStartUp() {
            _newActiveLayer = null;
        }

        private void HandlerInput(IGuiComponent layer) {
            if (Event.current.type == EventType.MouseDown && layer.Area.Contains(Event.current.mousePosition)) {
                _newActiveLayer = layer;
            }    
        }

        private void CheckActiveLayer() {
            if (_newActiveLayer == null) return;

            _layers.Remove(_newActiveLayer);
            _layers.Add(_newActiveLayer);

            foreach (var layer in _layers)
            {
                layer.Active = false;
            }

            _newActiveLayer.Active = true;
            _parent.CurrentLayer = _newActiveLayer.Transform;
        }

        private void CheckDeleteLayer() {
            IGuiComponent deletedComponent = _layers.FirstOrDefault(layer => !layer.Alive);

            if (deletedComponent == null) return;

            _layers.Remove(deletedComponent);
            Object.DestroyImmediate(deletedComponent.Transform.gameObject);
        }

        private void CreateLayerPanel()
        {
            if (EtDGUIUtils.Button(EtDEditorDictionary.GetPhrase(EtDEditorDictionary.AddCommand, EtDEditorDictionary.Layer),
                                   Color.yellow, Screen.width * 0.3f)) {
                AddLayer();
            }


            if (EtDGUIUtils.Button("Test interval", Color.yellow, Screen.width * 0.3f)) {
                 _parent.TileSets.Clear();
                 EditorUtility.SetDirty(_parent);


                Debug.Log(_parent.Intervals.UniqueNumber(EtDIntervals.TileSetInterval));

                //_parent.Intervals.Clear();
            }

            if (EtDGUIUtils.Button("Clear interval", Color.yellow, Screen.width * 0.3f)) {

                var list = new List<int>() {1, 2, 3};

                list.Insert(2, 5);

                EtDPrintUtils.Print(list);

            }
        }


        private void AddLayer() {
            Transform layer = _parent.CreateLayer();

            IGuiComponent newLayer = new LayerDraggableItem(LayerHeight, layer, this);

            if (_layers.Count > 0)
            {
                IGuiComponent lastLayer = _layers.Last();
                Vector3 lastLayerPosition = lastLayer.Transform.localPosition;
                Vector3 layerLocalPosition = newLayer.Transform.localPosition;

                newLayer.Position = new Vector2(0, lastLayer.Position.y + LayerHeight);
                newLayer.Transform.localPosition = new Vector3(layerLocalPosition.x, layerLocalPosition.y, lastLayerPosition.z + DeltaZ);
            }

            _layers.Add(newLayer);

        }

        private void AddLayer(Transform layer) {
            _layers.Add(new LayerDraggableItem(LayerHeight, layer, this));
        }

        public string RenameLayer(Transform layer, string newName) {
            if (_parent.LayerExists(newName)) return layer.gameObject.name;

            layer.gameObject.name = newName;
            return newName;
        }


        public class DraggbleComparer : IComparer<IGuiComponent> {
            public int Compare(IGuiComponent a, IGuiComponent b)
            {
                return a.Position.y.CompareTo(b.Position.y);
            }
        }


        public class LayerZComparer : IComparer<IGuiComponent>
        {
            public int Compare(IGuiComponent a, IGuiComponent b)
            {

                float z1 = a.Transform.localPosition.z;
                float z2 = b.Transform.localPosition.z;

                return z1.CompareTo(z2);
            }
        }




        // Pallete area
        private IntVector2 _lastTileOnPallete;

        public IntVector2 LastTileOnPallete
        {
            get
            {
                if (_lastTileOnPallete == null) _lastTileOnPallete = new IntVector2();

                if (_parent.CurrentTileSetIndex < 0 || _parent.CurrentTileSetIndex >= _parent.TileSets.Count) return _lastTileOnPallete;
                EtDTileSet tileSet = _parent.TileSets[_parent.CurrentTileSetIndex];
                if (tileSet == null) return _lastTileOnPallete;

                Texture tileSetTexture = tileSet.MainTexture;
                float palleteScale = _parent.PalleteScale;

                var viewRect = new Rect(0, 0, tileSetTexture.width, tileSetTexture.height);
                viewRect.width *= palleteScale;
                viewRect.height *= palleteScale;

                _lastTileOnPallete.x = (int)(viewRect.width / _parent.EditorDrawCellSize.x) - 1;
                _lastTileOnPallete.y = (int)(viewRect.height / _parent.EditorDrawCellSize.y) - 1;

                return _lastTileOnPallete;
            }
        }

        private EtDTileSet CurrentTileSet {
            get {
                if (_parent.CurrentTileSetIndex < 0 || _parent.CurrentTileSetIndex >= _parent.TileSets.Count) return null;
                return _parent.TileSets[_parent.CurrentTileSetIndex];
            }
        }


        private void DrawPallete() {

            if (_parent.NoMaterials) {
                GUILayout.Box(EtDEditorDictionary.GetWord(EtDEditorDictionary.NoMaterials));
                return;
            }

            _parent.CurrentTileSetIndex = EditorGUILayout.Popup(EtDEditorDictionary.GetWord(EtDEditorDictionary.Tileset), _parent.CurrentTileSetIndex, _parent.TileSetNames);
            _parent.PalleteScale = EditorGUILayout.Slider(EtDEditorDictionary.GetWord(EtDEditorDictionary.PalleteScale), _parent.PalleteScale, EtDTileMap.MinScale, EtDTileMap.MaxScale);

            if (_parent.CurrentTileSetIndex < 0 || _parent.CurrentTileSetIndex >= _parent.TileSets.Count) return;

            EtDTileSet tileSet = _parent.TileSets[_parent.CurrentTileSetIndex];
            if (tileSet == null) return;

            Texture tileSetTexture = tileSet.MainTexture;
            float palleteScale = _parent.PalleteScale;

            var viewRect = new Rect(0, 0, tileSetTexture.width, tileSetTexture.height);
            viewRect.width *= palleteScale;
            viewRect.height *= palleteScale;

            _palettePos = EditorGUILayout.BeginScrollView(_palettePos);

            UnityEngine.GUI.DrawTexture(viewRect, tileSetTexture, ScaleMode.StretchToFill, true, palleteScale - 1);
            GUILayoutUtility.GetRect(viewRect.width, viewRect.height);

            DrawSelectedTiles(GUILayoutUtility.GetLastRect());

            EditorGUILayout.EndScrollView();

            HandleInputPallete(GUILayoutUtility.GetLastRect());
        }

        private void DrawSelectedTiles(Rect area) {
            if (_tileFirst.x == -1 || _tileFirst.x == -1) return;

            Vector2 cellSize = _parent.EditorDrawCellSize;
            var position = new IntVector2(System.Math.Min(_tileFirst.x, _tileLast.x), System.Math.Min(_tileFirst.y, _tileLast.y));

            int cellsX = System.Math.Abs(_tileFirst.x - _tileLast.x) + 1;
            int cellsY = System.Math.Abs(_tileFirst.y - _tileLast.y) + 1;

            var highlightRect = new Rect(area.x + position.x * cellSize.x,
                                         area.y + position.y * cellSize.y,
                                         cellSize.x * cellsX,
                                         cellSize.y * cellsY);

            EtDDrawUtils.DrawSolidRectangleWithOutline(highlightRect); 
        }

        private void HandleInputPallete(Rect area) {

            int controlId = GUIUtility.GetControlID(FocusType.Passive, area);
            Event e = Event.current;

            if (!area.Contains(e.mousePosition)) return;

            Vector2 localClickPosition = e.mousePosition -
                                         new Vector2(area.x, area.y) + new Vector2(_palettePos.x, _palettePos.y);

            var tileLocalPosition = new IntVector2((int)(localClickPosition.x / _parent.EditorDrawCellSize.x),
                                                   (int)(localClickPosition.y / _parent.EditorDrawCellSize.y));

            switch (e.GetTypeForControl(controlId))
            {
                case EventType.MouseDown:

                    _tileFirst.Set(tileLocalPosition);
                    _tileLast.Set(tileLocalPosition);

                    // Memorize data for templates
                    TileMaker.SelectStartOnPallete = tileLocalPosition;
                    TileMaker.SelectEndOnPallete = tileLocalPosition;

                    TileMaker.Material = CurrentTileSet.Material;

                    HandleUtility.Repaint();
                    GUIUtility.hotControl = controlId;

                break;

                case EventType.MouseDrag:

                    if (GUIUtility.hotControl == controlId) {
                        _tileLast.Set(tileLocalPosition);

                        _tileLast.x = Mathf.Clamp(_tileLast.x, 0, LastTileOnPallete.x);
                        _tileLast.y = Mathf.Clamp(_tileLast.y, 0, LastTileOnPallete.y);

                        TileMaker.SelectEndOnPallete = _tileLast;

                        HandleUtility.Repaint();
                    }
                    break;

                case EventType.MouseUp:

                    if (GUIUtility.hotControl == controlId) {
                        GUIUtility.hotControl = 0;
                        HandleUtility.Repaint();

                        TileMaker.SelectEndOnPallete = _tileLast;

                        // Build tile maker
                        TileMaker.BuildMesh();
                    }

                    break;
            }

        }


        public void OnSceneGUI() {
            Event e = Event.current;


            if (!EditorWindow.mouseOverWindow) return;
                Vector3 coords = EtDInputUtils.GetMouseCoordinates(e);

            if (!_editor.TileMaker.StartDrag)
                TileMaker.ToGrid(coords, _parent.TileSize);

             HadleDrawInput(e, coords);
        }

        private void HadleDrawInput(Event e, Vector3 coords) {

            Vector2 tileSize = _parent.TileSize;
            var positionOnGrid = new IntVector2(Mathf.FloorToInt(coords.x / tileSize.x), Mathf.FloorToInt(coords.y / tileSize.y) * -1);

            int controlId = _parent.TileMapHashCode;

            switch (e.type) {
                
                case EventType.MouseDown:

                    GUIUtility.hotControl = controlId;

                    if (TileMaker.CheckState(EtDTileMaker.States.Draw)) {
                        TileMaker.Draw(_parent.CurrentLayer);
                    }

                    if (TileMaker.CheckState(EtDTileMaker.States.MassDraw) || TileMaker.CheckState(EtDTileMaker.States.Erase)) {

                        TileMaker.SelectStart = new IntVector2(positionOnGrid);
                        TileMaker.SelectEnd = new IntVector2(positionOnGrid);

                        TileMaker.StartDrag = true;
                    }

                    break;

                case EventType.MouseDrag:

                    if (TileMaker.CheckState(EtDTileMaker.States.Draw))
                        TileMaker.Draw(_parent.CurrentLayer);

                    if (TileMaker.CheckState(EtDTileMaker.States.MassDraw) ||
                        TileMaker.CheckState(EtDTileMaker.States.Erase)) {

                        TileMaker.SelectEnd = positionOnGrid;
                        HandleUtility.Repaint();
                    }

                    if (TileMaker.CheckState(EtDTileMaker.States.MassDraw))
                        TileMaker.BuildMesh();

                    break;

                case EventType.MouseUp:

                    GUIUtility.hotControl = 0;

                    if (TileMaker.CheckState(EtDTileMaker.States.Erase)) {
                        TileMaker.EraseTiles(_parent.CurrentLayer);
                    } 

                    TileMaker.SelectEnd = TileMaker.SelectStart;
                    TileMaker.SelectEndOld = TileMaker.SelectStart;

                    TileMaker.StartDrag = false;

                    if (TileMaker.CheckState(EtDTileMaker.States.MassDraw)) {
                        TileMaker.Draw(_parent.CurrentLayer);
                        TileMaker.RebuildSelectedTiles();
                    }

                break;


                case EventType.KeyDown:
                    if (e.keyCode == KeyCode.LeftControl) {
                        TileMaker.ChangeState(EtDTileMaker.States.Erase);
                    }

                    if (e.keyCode == KeyCode.Space) {
                        TileMaker.ChangeState(EtDTileMaker.States.MassDraw);
                    }

                    break;

                case EventType.KeyUp:
                    if (e.keyCode == KeyCode.LeftControl || e.keyCode == KeyCode.Space) {
                        TileMaker.ChangeState(EtDTileMaker.States.Draw);
                    }

                    break;


            }

        }
            
       


// ReSharper disable once InconsistentNaming
// ReSharper disable once UnusedMember.Local
        private void LovelyGL() {
            Rect viewport = EtDGUIUtils.GetRect("Pallete", 100, 300);

            GLDraw.BeginGroup(viewport);

            GLDraw.DrawBox(new Vector2(10, 10), new Vector2(70, 70), Color.red, 3f);

            GLDraw.DrawConnectingCurve(new Vector2(10, 10), new Vector2(70, 70), Color.yellow, 3f);

            GLDraw.DrawBox(new Vector2(70, 70), new Vector2(130, 130), Color.red, 3f);

            GLDraw.EndGroup();
        }


    }
}
