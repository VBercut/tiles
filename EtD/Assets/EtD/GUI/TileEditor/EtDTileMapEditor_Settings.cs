﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;

namespace Assets.EtD.GUI.TileEditor
{
    public class EtDTileMapEditorSettings : ISubEditor
    {
        private EtDTileMap _parent;

        public EtDTileMapEditorSettings(EtDTileMap parent)
        {
            _parent = parent;
        }

        public void OnInspectorGUI() {

            //GUILayout.FlexibleSpace();

            GUILayout.BeginVertical();

            _parent.GridColor = EditorGUILayout.ColorField(EtDEditorDictionary.GetWord(EtDEditorDictionary.GridColor), _parent.GridColor);

            EtDGUIUtils.HorizontalDiviner(5);

            _parent.Size = EditorGUILayout.Vector2Field(EtDEditorDictionary.GetWord(EtDEditorDictionary.Size), _parent.Size);
            _parent.ShowGrid = EditorGUILayout.Toggle(EtDEditorDictionary.GetWord(EtDEditorDictionary.GridColor),
                                                      _parent.ShowGrid);

            GUILayout.EndVertical();

        }

        public void OnSceneGUI()
        {
        }
    }
}
