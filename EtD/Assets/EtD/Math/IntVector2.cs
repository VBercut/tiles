﻿using System;
using UnityEngine;

namespace Assets.EtD.Math {


    [Serializable]
    public class IntVector2
    {
        public int x;
        public int y;

        public IntVector2(): this(0, 0)
        {
        }

        public IntVector2(IntVector2 vector2) : this(vector2.x, vector2.y)
        {

        }

        public IntVector2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void Set(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void Set(IntVector2 vector2)
        {
            x = vector2.x;
            y = vector2.y;
        }

        int SqrMagnitude
        {
            get { return x * x + y * y; }
        }

        public int Multiply
        {
            get { return x * y; }
        }

        public static Vector2 operator / (IntVector2 vector2, float scalar) {
            return new Vector2(vector2.x / scalar, vector2.y / scalar);
        }

        public static IntVector2 operator / (IntVector2 vector2, int scalar)
        {
            return new IntVector2(vector2.x / scalar, vector2.y / scalar);
        }

        public static IntVector2 operator *(IntVector2 vector2, int scalar)
        {
            return new IntVector2(vector2.x * scalar, vector2.y * scalar);
        }

        public static IntVector2 operator -(IntVector2 vector2, int scalar) {
            return new IntVector2(vector2.x - scalar, vector2.y - scalar);
        }

        public override string ToString()
        {
            return "x: " + x + " y: " + y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is IntVector2)) return false;

            var v = (IntVector2) obj;

            return v.x == x && v.y == y;
        }

        public bool Zero
        {
            get { return x == 0 && y == 0; }
        }

        public bool PartiallyZero {
            get { return x == 0 || y == 0; }
        }

        public bool IsInvertDirection(IntVector2 v) {
            return System.Math.Sign(x) != System.Math.Sign(v.x) || System.Math.Sign(y) != System.Math.Sign(v.y);
        }

        public bool Compare(int x, int y)
        {
            return this.x == x && this.y == y;
        }


        public override int GetHashCode() {
            return ShiftAndWrap(x.GetHashCode(), 2) ^ y.GetHashCode();
        }

        private int ShiftAndWrap(int value, int positions)
        {
            positions = positions & 0x1F;

            // Save the existing bit pattern, but interpret it as an unsigned integer.
            uint number = BitConverter.ToUInt32(BitConverter.GetBytes(value), 0);
            // Preserve the bits to be discarded.
            uint wrapped = number >> (32 - positions);
            // Shift and wrap the discarded bits.
            return BitConverter.ToInt32(BitConverter.GetBytes((number << positions) | wrapped), 0);
        }
    }
}
