﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Assets.EtD.Math;
using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;

namespace Assets.EtD {
    class Layer : MonoBehaviour {

        public const int Empty = -2;
        public const int Tile = -1;

        [SerializeField]
        private List<LayerTile> _tiles;

        public List<LayerTile> Tiles {
            get { return _tiles ?? (_tiles = new List<LayerTile>()); }
        }

        public int NextMesh;

        private EtDTileMap _tileMap;

        public EtDTileMap TileMap {
            get { return _tileMap ?? (_tileMap = transform.root.GetComponent<EtDTileMap>()); }
        }

        public string NextMeshName {
            get { return new StringBuilder("TileMapMesh").Append(++NextMesh).ToString(); }
        }

        private Dictionary<IntVector2, LayerTile> _positionDictionary;

        public Dictionary<IntVector2, LayerTile> PositionDictionary {
            get {
                if (_positionDictionary == null) {
                    _positionDictionary = new Dictionary<IntVector2, LayerTile>();

                    foreach (var tile in Tiles) {
                        _positionDictionary[tile.PositionOnMap] = tile;
                    }
                }
                return _positionDictionary;
            }
        }

        private Dictionary<IntVector2, TileMapMesh> _positionMeshDictionary;

        public Dictionary<IntVector2, TileMapMesh> PositionMeshDictionary {
            get
            {
                if (_positionMeshDictionary == null)
                {
                    _positionMeshDictionary = new Dictionary<IntVector2, TileMapMesh>();

                    foreach (Transform child in transform) {
                        var tileMapMesh = child.gameObject.GetComponent<TileMapMesh>();
                        if (tileMapMesh == null) continue;

                        foreach (var position in tileMapMesh.Positions) {
                            _positionMeshDictionary[position] = tileMapMesh;
                        }

                        _materialMeshDictionary[tileMapMesh.Material] = tileMapMesh;
                    }
                }
                return _positionMeshDictionary;
            }
        }

        private Dictionary<Material, TileMapMesh> _materialMeshDictionary;

        public Dictionary<Material, TileMapMesh> MaterialMeshDictionary {
            get {
                if (_materialMeshDictionary == null) {
                    _materialMeshDictionary = new Dictionary<Material, TileMapMesh>();

                    foreach (Transform child in transform) {
                        var tileMapMesh = child.gameObject.GetComponent<TileMapMesh>();
                        if(tileMapMesh == null) continue;
                        if (tileMapMesh.Material == null) continue;
                        _materialMeshDictionary[tileMapMesh.Material] = tileMapMesh;
                    }

                }
                return _materialMeshDictionary;
            }
        }


        private static readonly int[] LeftTopCornerTiles = {0, 1, 3, 4};
        private static readonly int[] RightTopCornerTiles = {1, 2, 4, 5};

        private static readonly int[] LeftBottomCornerTiles = {3, 4, 6, 7};
        private static readonly int[] RightBottomCornerTiles = {4, 5, 7, 8};

        public void AddTile(Tile tile, IntVector2 positionOnMap) {

            var cornerTilePosition = new IntVector2(positionOnMap.x * 2, positionOnMap.y * 2);
            var cornerTileSize = new IntVector2(TileMap.CellSize.x / 2, TileMap.CellSize.y / 2);

            // Для начала ищем айди тайла, чтобы охарактеризовать его
            EtDAutoTileTemplate autoTileTemplate = TileMap.FindTemplate(tile.TexturePosition);
            int tileID = autoTileTemplate != null ? autoTileTemplate.ID : Tile;

            var newTile = new LayerTile(tileID, tile.Material,
                                        new IntVector2(tile.TexturePosition.x * 2, tile.TexturePosition.y * 2), positionOnMap,
                                        new IntVector2(TileMap.CellSize.x / 2, TileMap.CellSize.y / 2));

            // Проверяем, существует какой-либо тайл на карте
            if (PositionDictionary.ContainsKey(positionOnMap)) {
                LayerTile oldTile = PositionDictionary[positionOnMap];
                Tiles.Remove(oldTile);
                PositionDictionary.Remove(positionOnMap);
            }

            PositionDictionary[positionOnMap] = newTile;
            Tiles.Add(newTile);

            // -- 0 -- -- 1 -- -- 2 --
            // -- 3 -- -- 4 -- -- 5 --
            // -- 6 -- -- 7 -- -- 8 --

            var otherTiles = new List<LayerTile> {
                FindLayerTile(new IntVector2(positionOnMap.x - 1, positionOnMap.y - 1)),  // 0
                FindLayerTile(new IntVector2(positionOnMap.x, positionOnMap.y - 1)),      // 1
                FindLayerTile(new IntVector2(positionOnMap.x + 1, positionOnMap.y - 1)),  // 2

                FindLayerTile(new IntVector2(positionOnMap.x - 1, positionOnMap.y)),      // 3
                FindLayerTile(new IntVector2(positionOnMap.x, positionOnMap.y)),          // 4
                FindLayerTile(new IntVector2(positionOnMap.x + 1, positionOnMap.y)),      // 5

                FindLayerTile(new IntVector2(positionOnMap.x - 1, positionOnMap.y + 1)),  // 6
                FindLayerTile(new IntVector2(positionOnMap.x, positionOnMap.y + 1)),      // 7
                FindLayerTile(new IntVector2(positionOnMap.x + 1, positionOnMap.y + 1))   // 8
            };



            // Left Top tileCorner 0, 1, 3, 4
            Template template = Template.CreateTemplate(LeftTopCornerTiles, otherTiles, tileID);

            var correctPair = TileMap.FindPair(template.First, template.Second);
            bool templateExists = template.Exists && correctPair != null;

            if (templateExists) {

                EtDTileContainer tileContainer = correctPair.FindTile(template.Result, tileID);

                //LeftTopCorner
                var leftTopCornerPosition = new IntVector2(cornerTilePosition.x - 1, cornerTilePosition.y - 1);
                SetTile(leftTopCornerPosition, tileContainer.LeftTopCorner.Material, tileContainer.LeftTopCorner.Mesh.uv);

                //RightTopCorner
                var rightTopCornerPosition = new IntVector2(cornerTilePosition.x, cornerTilePosition.y - 1);
                SetTile(rightTopCornerPosition, tileContainer.RightTopCorner.Material, tileContainer.RightTopCorner.Mesh.uv);

                //LeftBottomCorner
                var leftBottomCornerPosition = new IntVector2(cornerTilePosition.x - 1, cornerTilePosition.y);
                SetTile(leftBottomCornerPosition, tileContainer.LeftBottomCorner.Material, tileContainer.LeftBottomCorner.Mesh.uv);

                //RightTopCorner
                var rightBottomCornerPosition = new IntVector2(cornerTilePosition.x, cornerTilePosition.y);
                SetTile(rightBottomCornerPosition, tileContainer.RightBottomCorner.Material, tileContainer.RightBottomCorner.Mesh.uv);
            } else {

                    //LeftTopCorner
                    var leftTopCornerPosition = new IntVector2(cornerTilePosition.x - 1, cornerTilePosition.y - 1);
                    SetTile(leftTopCornerPosition, otherTiles, tile: 0, corner: 3, mainID: tileID);

                    //RightTopCorner
                    var rightTopCornerPosition = new IntVector2(cornerTilePosition.x, cornerTilePosition.y - 1);
                    SetTile(rightTopCornerPosition, otherTiles, tile: 1, corner: 2, mainID: tileID);

                    //LeftBottomCorner
                    var leftBottomCornerPosition = new IntVector2(cornerTilePosition.x - 1, cornerTilePosition.y);
                    SetTile(leftBottomCornerPosition, otherTiles, tile: 3, corner: 1, mainID: tileID);

                //RightTopCorner
                var rightBottomCornerPosition = new IntVector2(cornerTilePosition.x, cornerTilePosition.y);
                SetTile(rightBottomCornerPosition, otherTiles, tile: 4, corner: 0); // center
            }


            // Right Top tileCorner 1, 2, 4, 5
            template = Template.CreateTemplate(RightTopCornerTiles, otherTiles, tileID);

            correctPair = TileMap.FindPair(template.First, template.Second);
            templateExists = template.Exists && correctPair != null;

            if (templateExists) {

                EtDTileContainer tileContainer = correctPair.FindTile(template.Result, tileID);

                //LeftTopCorner
                var leftTopCornerPosition = new IntVector2(cornerTilePosition.x + 1, cornerTilePosition.y - 1);
                SetTile(leftTopCornerPosition, tileContainer.LeftTopCorner.Material, tileContainer.LeftTopCorner.Mesh.uv);

                //RightTopCorner
                var rightTopCornerPosition = new IntVector2(cornerTilePosition.x + 2, cornerTilePosition.y - 1);
                SetTile(rightTopCornerPosition, tileContainer.RightTopCorner.Material, tileContainer.RightTopCorner.Mesh.uv);

                //LeftBottomCorner
                var leftBottomCornerPosition = new IntVector2(cornerTilePosition.x + 1, cornerTilePosition.y);
                SetTile(leftBottomCornerPosition, tileContainer.LeftBottomCorner.Material, tileContainer.LeftBottomCorner.Mesh.uv);

                //RightTopCorner
                var rightBottomCornerPosition = new IntVector2(cornerTilePosition.x + 2, cornerTilePosition.y);
                SetTile(rightBottomCornerPosition, tileContainer.RightBottomCorner.Material, tileContainer.RightBottomCorner.Mesh.uv);
            }
            else
            {
               //LeftTopCorner
               var leftTopCornerPosition = new IntVector2(cornerTilePosition.x + 1, cornerTilePosition.y - 1);
               SetTile(leftTopCornerPosition, otherTiles, tile: 1, corner: 3, mainID: tileID);

               //RightTopCorner
               var rightTopCornerPosition = new IntVector2(cornerTilePosition.x + 2, cornerTilePosition.y - 1);
               SetTile(rightTopCornerPosition, otherTiles, tile: 2, corner: 2, mainID: tileID);

               //RightTopCorner
               var rightBottomCornerPosition = new IntVector2(cornerTilePosition.x + 2, cornerTilePosition.y);
               SetTile(rightBottomCornerPosition, otherTiles, tile: 5, corner: 0, mainID: tileID); // center 

                //LeftBottomCorner
                var leftBottomCornerPosition = new IntVector2(cornerTilePosition.x + 1, cornerTilePosition.y);
                SetTile(leftBottomCornerPosition, otherTiles, tile: 4, corner: 1);
            }


            // Left Bottom tileCorner 3, 4, 6, 7
            template = Template.CreateTemplate(LeftBottomCornerTiles, otherTiles, tileID);

            correctPair = TileMap.FindPair(template.First, template.Second);
            templateExists = template.Exists && correctPair != null;

            if (templateExists) {
                EtDTileContainer tileContainer = correctPair.FindTile(template.Result, tileID);

                //LeftTopCorner
                var leftTopCornerPosition = new IntVector2(cornerTilePosition.x - 1, cornerTilePosition.y + 1);
                SetTile(leftTopCornerPosition, tileContainer.LeftTopCorner.Material, tileContainer.LeftTopCorner.Mesh.uv);

                //RightTopCorner
                var rightTopCornerPosition = new IntVector2(cornerTilePosition.x, cornerTilePosition.y + 1);
                SetTile(rightTopCornerPosition, tileContainer.RightTopCorner.Material, tileContainer.RightTopCorner.Mesh.uv);

                //LeftBottomCorner
                var leftBottomCornerPosition = new IntVector2(cornerTilePosition.x - 1, cornerTilePosition.y + 2);
                SetTile(leftBottomCornerPosition, tileContainer.LeftBottomCorner.Material, tileContainer.LeftBottomCorner.Mesh.uv);

                //RightTopCorner
                var rightBottomCornerPosition = new IntVector2(cornerTilePosition.x, cornerTilePosition.y + 2);
                SetTile(rightBottomCornerPosition, tileContainer.RightBottomCorner.Material, tileContainer.RightBottomCorner.Mesh.uv);
            }
            else
            {

                //LeftTopCorner
                var leftTopCornerPosition = new IntVector2(cornerTilePosition.x - 1, cornerTilePosition.y + 1);
                SetTile(leftTopCornerPosition, otherTiles, tile: 3, corner: 3, mainID: tileID);

                //LeftBottomCorner
                var leftBottomCornerPosition = new IntVector2(cornerTilePosition.x - 1, cornerTilePosition.y + 2);
                SetTile(leftBottomCornerPosition, otherTiles, tile: 6, corner: 1, mainID: tileID);

                //RightBottomCorner
                var rightBottomCornerPosition = new IntVector2(cornerTilePosition.x, cornerTilePosition.y + 2);
                SetTile(rightBottomCornerPosition, otherTiles, tile: 7, corner: 0, mainID: tileID);  

                //RightTopCorner
                var rightTopCornerPosition = new IntVector2(cornerTilePosition.x, cornerTilePosition.y + 1);
                SetTile(rightTopCornerPosition, otherTiles, tile: 4, corner: 2);

            }


            // Right Bottom tileCorner 4, 5, 7, 8
            template = Template.CreateTemplate(RightBottomCornerTiles, otherTiles, tileID);

            correctPair = TileMap.FindPair(template.First, template.Second);
            templateExists = template.Exists && correctPair != null;

            if (templateExists)
            {
                EtDTileContainer tileContainer = correctPair.FindTile(template.Result, tileID);

                //LeftTopCorner
                var leftTopCornerPosition = new IntVector2(cornerTilePosition.x + 1, cornerTilePosition.y + 1);
                SetTile(leftTopCornerPosition, tileContainer.LeftTopCorner.Material, tileContainer.LeftTopCorner.Mesh.uv);

                //RightTopCorner
                var rightTopCornerPosition = new IntVector2(cornerTilePosition.x + 2, cornerTilePosition.y + 1);
                SetTile(rightTopCornerPosition, tileContainer.RightTopCorner.Material, tileContainer.RightTopCorner.Mesh.uv);

                //LeftBottomCorner
                var leftBottomCornerPosition = new IntVector2(cornerTilePosition.x + 1, cornerTilePosition.y + 2);
                SetTile(leftBottomCornerPosition, tileContainer.LeftBottomCorner.Material, tileContainer.LeftBottomCorner.Mesh.uv);

                //RightTopCorner
                var rightBottomCornerPosition = new IntVector2(cornerTilePosition.x + 2, cornerTilePosition.y + 2);
                SetTile(rightBottomCornerPosition, tileContainer.RightBottomCorner.Material, tileContainer.RightBottomCorner.Mesh.uv);
            } else {

                //LeftTopCorner
                var leftTopCornerPosition = new IntVector2(cornerTilePosition.x + 1, cornerTilePosition.y + 1);
                SetTile(leftTopCornerPosition, otherTiles, tile: 4, corner: 3);

                //RightTopCorner
                var rightTopCornerPosition = new IntVector2(cornerTilePosition.x + 2, cornerTilePosition.y + 1);
                SetTile(rightTopCornerPosition, otherTiles, tile: 5, corner: 2, mainID: tileID);

                //LeftBottomCorner
                var leftBottomCornerPosition = new IntVector2(cornerTilePosition.x + 1, cornerTilePosition.y + 2);
                SetTile(leftBottomCornerPosition, otherTiles, tile: 7, corner: 1, mainID: tileID);

                //RightTopCorner
                var rightBottomCornerPosition = new IntVector2(cornerTilePosition.x + 2, cornerTilePosition.y + 2);
                SetTile(rightBottomCornerPosition, otherTiles, tile: 8, corner: 0, mainID: tileID); 
            }


        }

        private void SetTile(IntVector2 pos, List<LayerTile> otherTiles, int tile, int corner, int mainID) {
            LayerTile otherTile = otherTiles[tile];
            int tileID = otherTile != null ? otherTile.ID : Empty;
            if(tileID != mainID) return;
            SetTile(pos, otherTiles, tile, corner);
        }

        private void SetTile(IntVector2 position, List<LayerTile> otherTiles, int tile, int corner) {
            LayerTile layerTile = otherTiles[tile];
            if(layerTile == null) return;
            SetTile(position, layerTile.Material, layerTile.GetCornerUV(corner));
        }

        public TileMapMesh FindMesh(Material material) {
            TileMapMesh tileMapMesh;

            if (material == null) return null;

            if (!MaterialMeshDictionary.ContainsKey(material)) {
                var meshTransform = EtDGameObjectUtils.AddChild(transform, NextMeshName);
                tileMapMesh = meshTransform.gameObject.AddComponent<TileMapMesh>();
                tileMapMesh.MeshRenderer.sharedMaterial = material;
                MaterialMeshDictionary[material] = tileMapMesh;
            } else {
                tileMapMesh = MaterialMeshDictionary[material];
            }

            return tileMapMesh;
        }

        public void SetTile(IntVector2 position, Material material, Vector2[] uv) {
            if (!CanPlaceTile(position)) return;
            TileMapMesh mesh = FindMesh(material);
            if(mesh == null) return;
            mesh.AddTile(position, uv, TileMap.TileSize / 2); 
        }


        public bool CanPlaceTile(IntVector2 pos) {
            return !(pos.x < 0 || pos.y < 0 || pos.x >= TileMap.DoubleWidth || pos.y >= TileMap.DoubleHeight);
        }

        public LayerTile FindLayerTile(IntVector2 position) {
            if(!PositionDictionary.ContainsKey(position)) return null;
            return PositionDictionary[position];
        }


        [Serializable]
        public class LayerTile {
            public int ID;
            public int MaterialID;

            public IntVector2 PositionOnMap;

            public IntVector2 LeftTopPosition {
                get { return PositionOnMap; }
            }

            [SerializeField]
            private IntVector2 _rightTopPosition;

            public IntVector2 RightTopPosition {
                get { return _rightTopPosition ?? (_rightTopPosition = new IntVector2(LeftTopPosition.x + 1, LeftTopPosition.y)); }
            }

            [SerializeField]
            private IntVector2 _leftBottomPosition;

            public IntVector2 LeftBottomPosition {
                get { return _leftBottomPosition ?? (_leftBottomPosition = new IntVector2(LeftTopPosition.x, LeftTopPosition.y + 1)); }
            }

            [SerializeField]
            private IntVector2 _rightBottomPosition;

            public IntVector2 RightBottomPosition {
                get { return _rightBottomPosition ?? (_rightBottomPosition = new IntVector2(LeftTopPosition.x + 1, LeftTopPosition.y + 1)); }
            }

            public Vector2[] LeftTopCornerUV;
            public Vector2[] RightTopCornerUV;
            public Vector2[] LeftBottomCornerUV;
            public Vector2[] RightBottomCornerUV;

            public Vector2[] GetCornerUV(int corner) {
                if (corner == 0) return LeftTopCornerUV;
                if (corner == 1) return RightTopCornerUV;
                if (corner == 2) return LeftBottomCornerUV;
                if (corner == 3) return RightBottomCornerUV;

                return LeftTopCornerUV;
            }


            private Material _material;

            public Material Material {
                get { return _material ?? (_material = EditorUtility.InstanceIDToObject(MaterialID) as Material); }
            }

            public LayerTile(int id, Material material, IntVector2 positionOnMaterial, IntVector2 positionOnMap, IntVector2 tileSize) {
                ID = id;
                MaterialID = material.GetInstanceID();
                PositionOnMap = new IntVector2(positionOnMap);

                LeftTopCornerUV = EtDMeshUtils.TextureCoordsUV(material.mainTexture, new IntVector2(positionOnMaterial.x, positionOnMaterial.y), tileSize);
                RightTopCornerUV = EtDMeshUtils.TextureCoordsUV(material.mainTexture, new IntVector2(positionOnMaterial.x + 1, positionOnMaterial.y), tileSize);
                LeftBottomCornerUV = EtDMeshUtils.TextureCoordsUV(material.mainTexture, new IntVector2(positionOnMaterial.x, positionOnMaterial.y + 1), tileSize);
                RightBottomCornerUV = EtDMeshUtils.TextureCoordsUV(material.mainTexture, new IntVector2(positionOnMaterial.x + 1, positionOnMaterial.y + 1), tileSize);
            }
        }


        private class Template
        {
            private int _first = Empty;
            private int _second = Empty;

            private List<int> _result;

            public bool Exists
            {
                get { return _result.Count > 0; }
            }

            public int First
            {
                get { return _first; }
            }

            public int Second
            {
                get { return _second; }
            }

            public IEnumerable<int> Result
            {
                get { return _result; }
            }

            public static Template CreateTemplate(IEnumerable<int> entires, List<LayerTile> tiles, int tileID) {
                var obj = new Template { _result = new List<int>() };
                var resultCount = new Dictionary<int, int>();

                foreach (var index in entires) {
                    var currentTile = tiles[index];

                    int currentId = currentTile == null ? tileID : currentTile.ID;

                    obj._result.Add(currentId);

                    if (!resultCount.ContainsKey(currentId)) {
                        resultCount[currentId] = 1;
                    } else {
                        resultCount[currentId]++;
                    }

                    if (resultCount.Count > 2) {
                        obj._result.Clear();
                        break;
                    }
                }

                int currentKey = 0;

                foreach (var key in resultCount.Keys) {
                    if (currentKey == 0) {
                        obj._first = obj._second = key;
                    }

                    if (currentKey == 1) {
                        obj._second = key;
                    }

                    currentKey++;
                }

                if (obj._first < 0 && obj._second < 0) {
                    obj._result.Clear();
                }

                if (obj._first >= 0 && obj._second < 0) {
                    obj._second = obj._first;
                }

                if (obj._first < 0 && obj._second >= 0) {
                    obj._first = obj._second;
                }

                return obj;
            }
        }

    }
}
