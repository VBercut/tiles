﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;

namespace Assets.SCommander.Scripts.Editor
{
    [CustomEditor(typeof(StaticFieldDrawer))]
    class StaticFieldDrawerEditor : UnityEditor.Editor {

        public override void OnInspectorGUI() {
            var drawer = (StaticFieldDrawer)target;

            string buttonGridText = !drawer.DrawMode ? EtDEditorDictionary.Draw : EtDEditorDictionary.SaveCommand;
            Color buttonGridColor = !drawer.DrawMode ? EtDGUIUtils.ExtraColor.Orange : EtDGUIUtils.ExtraColor.Blue;

            if (EtDGUIUtils.Button(buttonGridText, buttonGridColor)) {
                drawer.ChangeMode();
            }
        }

    }
}
