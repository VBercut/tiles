﻿using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;

namespace Assets.SCommander.Scripts.Editor {

    [CustomEditor(typeof(FlowFieldRender))]
    class FlowFieldRenderEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {

            var render = (FlowFieldRender) target;

            string buttonGridText = !render.ShowField ? EtDEditorDictionary.ShowCommand : EtDEditorDictionary.HideCommand;
            Color buttonGridColor = !render.ShowField ? EtDGUIUtils.ExtraColor.Orange : EtDGUIUtils.ExtraColor.Blue;

            if (EtDGUIUtils.Button(buttonGridText, buttonGridColor)) {
                render.ShowHideField();
            }
        }
    }
}
