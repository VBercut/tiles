﻿using Assets.EtD.Math;
using UnityEngine;

namespace Assets.SCommander.Scripts.Utils
{
    public class MaterialUtils {

        public static Texture2D CreateTexture2D(int width, int height, Color color) {
            var texture = new Texture2D(width, height, TextureFormat.ARGB32, false);
            var pixels = texture.GetPixels();

            for (int i = 0; i < pixels.Length; i++) {
                pixels[i] = color;
            }

            texture.SetPixels(pixels);
            texture.Apply();

            return texture;
        }

        public static Texture2D CreateTexture2D(int width, int height) {
            return CreateTexture2D(width, height, Color.clear);
        }


        public static void CreateCleanTexture(Material material, string textureName, int width, int height) {
            if (material.GetTexture(textureName) != null) return;

            material.SetTexture(textureName, CreateTexture2D(width, height));
        }
    }
}
