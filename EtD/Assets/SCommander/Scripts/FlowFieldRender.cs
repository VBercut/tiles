﻿using Assets.EtD.Math;
using UnityEngine;

namespace Assets.SCommander.Scripts {
    public class FlowFieldRender : MonoBehaviour {
        public bool ShowField;

        private FlowField _flowField;
        private MeshRenderer _meshRenderer;

        public FlowField FlowField {
            get { return _flowField ?? (_flowField = GetComponent<FlowField>()); }    
        }

        public void ShowHideField() {
            ShowField = !ShowField;

            var meshRender = GetComponent<MeshRenderer>();

            if(FlowField == null) return;
            if(meshRender == null) return;

            if (ShowField) {
                var texture = new Texture2D((int)FlowField.Tiles.x, (int)FlowField.Tiles.y, TextureFormat.ARGB32, false);
                var maxIteration = Mathf.Max(FlowField.Tiles.x, FlowField.Tiles.y) / 2;

                texture.hideFlags = HideFlags.DontSave;

                var center = new IntVector2(texture.width / 2, texture.height / 2);
                texture.SetPixel(center.x, center.y, Color.yellow);

                for (int i = 1; i <= maxIteration; i++) {
                    float t = i / maxIteration;
                    var currentColor = Color32.Lerp(Color.yellow, Color.red, t);

                    var horizontal = new IntVector2(center.x - i, center.x + i);
                    var vertical = new IntVector2(center.y - i, center.y + i);

                    // top
                    for (int x = horizontal.x; x <= horizontal.y; x++) {
                        if(vertical.x < 0) break;
                        if(x > texture.width) break;
                        if(x < 0) continue;

                        texture.SetPixel(x, vertical.x, currentColor);
                    }

                    // bottom
                    for (int x = horizontal.x; x <= horizontal.y; x++) {
                        if (vertical.y > texture.height) break;
                        if (x > texture.width) break;
                        if (x < 0) continue;

                        texture.SetPixel(x, vertical.y, currentColor);
                    }

                    // left
                    for (int y = vertical.x; y <= vertical.y; y++) {
                        if (horizontal.x < 0) break;
                        if (y > texture.height) break;
                        if (y < 0) continue;

                        texture.SetPixel(horizontal.x, y, currentColor);
                    }

                    // right
                    for (int y = vertical.x; y <= vertical.y; y++)
                    {
                        if (horizontal.y > texture.width) break;
                        if (y > texture.height) break;
                        if (y < 0) continue;

                        texture.SetPixel(horizontal.y, y, currentColor);
                    }
                }

                texture.Apply();
                GetComponent<MeshRenderer>().sharedMaterial.SetTexture("_FlowField", texture);
            }
        }
    }
}
