﻿using Assets.SCommander.Scripts.Utils;
using UnityEngine;

namespace Assets.SCommander.Scripts {

    [ExecuteInEditMode]
    public class FlowField : MonoBehaviour {

        public float TileSize;
        public Vector2 Tiles;
        public float FlowFieldAlpha = 0.3f;
        public byte[] StaticField;

        private float _oldTileSize;

        private const float MinTileSize = 0.01f;

        public void OnEnable() {
            Material material = GetComponent<MeshRenderer>().sharedMaterial;
            if (material == null) return;

            MaterialUtils.CreateCleanTexture(material, "_StaticField", (int)Tiles.x, (int)Tiles.y);
            MaterialUtils.CreateCleanTexture(material, "_FlowField", (int)Tiles.x, (int)Tiles.y); 
        }

        // Use this for initialization
        void Start () {
        }

        void OnInspectorGUI() {
        }
	
        // Update is called once per frame
        void Update () {
            Material material = GetComponent<MeshRenderer>().sharedMaterial;
            if (material == null) return;

            FlowFieldAlpha = Mathf.Clamp(FlowFieldAlpha, 0, 1);
            material.SetFloat("_BlendAlpha", FlowFieldAlpha);

            TileSize = Mathf.Clamp(TileSize, MinTileSize, Mathf.Min(transform.localScale.x, transform.localScale.z));
            Tiles.Set(transform.localScale.x / TileSize, transform.localScale.z / TileSize);

            // ReSharper disable CompareOfFloatsByEqualityOperator
            if (TileSize != _oldTileSize) {
            }

            _oldTileSize = TileSize;
        }

        public void DrawOnStaticField(RaycastHit hit) {
            Debug.Log("Draw: " + hit.point);
        }
    }
}
