﻿using Assets.EtD.Utils;
using UnityEditor;
using UnityEngine;

namespace Assets.SCommander.Scripts {
    [ExecuteInEditMode]
    public class StaticFieldDrawer : MonoBehaviour {

        public bool DrawMode;

        private RaycastHit _hit;

        public void Update() {
        }

        public FlowField DrawOnFlowField() {
            FlowField flowField = null;

            if (Input.GetMouseButton(0)) {
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit)) {
                    flowField = hit.transform.GetComponent<FlowField>();
                    _hit = hit;
                }
            }

            return flowField;
        }

        public void OnSceneGUI() {
/*            Debug.Log("test");
            FlowField flowField = DrawOnFlowField();
            if (flowField == null) return;

            flowField.DrawOnStaticField(_hit);*/


            Event e = Event.current;

            if (!EditorWindow.mouseOverWindow) return;
            Vector3 coords = EtDInputUtils.GetMouseCoordinates(e);

            GUIUtility.hotControl = DrawMode ? 1 : 0;
        }

        public void ChangeMode() {
            DrawMode = !DrawMode;

            GUIUtility.hotControl = DrawMode ? 1 : 0;
        }
    }
}
