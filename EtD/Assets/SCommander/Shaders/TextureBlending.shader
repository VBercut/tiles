﻿Shader "Custom/TextureBlending" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	    _StaticField ("Static Field(RGBA) ", 2D) = "white" {}
		_FlowField ("Flow Field (RGBA) ", 2D) = "white" {}
		_BlendAlpha ("Flow Field Alpha", Range (0,1)) = 1
	}

	SubShader {
		Pass {
		
				Blend SrcAlpha OneMinusSrcAlpha

				// Apply base texture
				SetTexture [_MainTex] {
					combine texture
				}

				SetTexture [_StaticField] {
					combine texture lerp (texture) previous
				}

				// Blend in the alpha texture using the lerp operator
				SetTexture [_FlowField] {
					constantColor (1, 1, 1, [_BlendAlpha])
					combine texture lerp (constant) previous
				}

        }
    }
	FallBack "Diffuse"
}
