﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

    public Transform Bounds;
    public Camera Camera;

    public Transform FollowObject;

    public float Speed;

    private float _maxX;
    private float _maxY;
    private float _minX;
    private float _minY;


    // Use this for initialization
    private void Start() { }

    // Update is called once per frame
    private void Update() {
        if (Camera == null) return;
        if (FollowObject == null) return;

        Vector3 t = Camera.transform.position;

        float deltaHorizontal = FollowObject.transform.position.x - t.x;
        float deltaVectical = FollowObject.transform.position.y - t.y;


        Camera.transform.position = new Vector3(t.x + deltaHorizontal * Time.deltaTime * Speed,
            t.y + deltaVectical * Time.deltaTime * Speed, t.z);


        if (Bounds != null)
        {
            float vertExtent = Camera.orthographicSize;
            float horzExtent = vertExtent * Screen.width / Screen.height;

            // Calculations assume map is position at the origin
            _minX = Bounds.position.x + horzExtent;
            _maxX = 9999999999;

            _minY = -9999999999;
            _maxY = Bounds.position.y - vertExtent;

        }
    }


    private void LateUpdate() {
        Vector3 v3 = transform.position;
        v3.x = Mathf.Clamp(v3.x, _minX, _maxX);
        v3.y = Mathf.Clamp(v3.y, _minY, _maxY);
        transform.position = v3;
    }
}
