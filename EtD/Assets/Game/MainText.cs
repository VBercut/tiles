﻿using UnityEngine;
using System.Collections;

public class MainText : MonoBehaviour {

    public float letterPause = 0.05f;
    public GUIStyle font;
    string message;
    string text;

	// Use this for initialization
	void Start () {

        message = "Welcome new warrior! This is a blue screen, you must fight your way out! " +
                  "Without any hands or other parts. This may take some time...";
        text = "";
        StartCoroutine(TypeText());
	
	}

    IEnumerator TypeText() {
        foreach (char letter in message.ToCharArray()) {
            text += letter;
            yield return new WaitForSeconds(letterPause);
        }
    }

    void OnGUI() {
        GUI.Label(new Rect(0, 0, 256, 1024), text, font);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            StopAllCoroutines();
            text = message;
        }
    }
}
