﻿using UnityEngine;
using System.Collections;

public class ObjectController : MonoBehaviour {

    public float Speed = 0;
    public float WalkSpeed = 1.0f;
    public float MaxRunSpeed = 4.5f;
    public float Acceleration = 0.3f;

    private static Shader _diffuseShader;

    public static Shader DiffuseShader
    {
        get { return _diffuseShader ?? (_diffuseShader = Shader.Find("Sprites/Diffuse")); }
    }

    private static Material _diffuseMaterial;

    public static Material DiffuseMaterial
    {
        get
        {
            return _diffuseMaterial ?? (_diffuseMaterial = new Material(DiffuseShader));
        }
    }

    private Animator _animator;

    // Use this for initialization
    private void Start()
    {
        var spriteRenderer = transform.GetComponent<SpriteRenderer>();
        _animator = transform.GetComponent<Animator>();

        var material = DiffuseMaterial;

        material.mainTexture = spriteRenderer.sprite.texture;
        spriteRenderer.sharedMaterial = DiffuseMaterial;
    }

    // Update is called once per frame
    private void Update()
    {
        /*        float deltaHorizontal = Input.GetAxis("Horizontal");
                float deltaVectical = Input.GetAxis("Vertical");

                Vector3 t = transform.position;

                transform.position = new Vector3(t.x + deltaHorizontal * Speed * Time.deltaTime,
                    t.y + deltaVectical * Speed * Time.deltaTime, t.z);*/
    }

    // Direction 0 - horizontal
    // Direction 1 - top
    // Direction 2 - bottom

    private bool _facingRight = true;
    private bool _running = false;
    private bool _death = false;

    private Vector2 dir;

    private void FixedUpdate()
    {

        if (_death) return;

        var direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (direction.SqrMagnitude() > 0)
        {
            dir = new Vector2(direction.x, direction.y);
        }

        if (direction.SqrMagnitude() > 0 && !_running)
        {
            Speed = Mathf.Max(Mathf.Abs(direction.x * WalkSpeed), Mathf.Abs(direction.y * WalkSpeed));
        }

        if (direction.SqrMagnitude() == 0)
            Speed = 0;


        if (Input.GetButton("Dush"))
        {
            Speed += Acceleration;
            _running = true;

            if (Speed > MaxRunSpeed)
            {
                Speed = MaxRunSpeed;
            }
        }

        if (Input.GetButtonUp("Dush"))
        {
            Speed = WalkSpeed;
            _running = false;
        }

        _animator.SetFloat("Speed", Speed);
        _animator.SetFloat("Direction", CalculateForAnimation(dir));

        rigidbody2D.velocity = new Vector2(direction.x * Speed, direction.y * Speed);

        if (direction.x > 0 && !_facingRight)
        {
            Flip();
        }
        else if (direction.x < 0 && _facingRight)
        {
            Flip();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        /*
        if (other.GetComponent<DeathZone>())
        {
            _animator.SetBool("Death", true);
            _death = true;
        }
         * */
    }

    private void Flip()
    {
        _facingRight = !_facingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;

        transform.localScale = theScale;
    }

    private const float HorizontalDirection = 0;
    private const float TopDirection = 0.5f;
    private const float BottomDirection = 1f;

    private float CalculateForAnimation(Vector2 directionInput)
    {
        if (directionInput.y > 0) return TopDirection;
        if (directionInput.y < 0) return BottomDirection;

        return HorizontalDirection;
    }
}
