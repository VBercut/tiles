﻿Shader "Custom/Sprite" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}

	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		LOD 200	
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off
		Pass {
			SetTexture [_MainTex] { combine texture }
		}
	} 

	FallBack "Diffuse"
}

